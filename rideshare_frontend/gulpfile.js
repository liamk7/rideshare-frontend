var gulp = require('gulp'),
    sass = require('gulp-sass'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    cleanCSS = require('gulp-clean-css'),
    htmlmin = require('gulp-htmlmin'),
    del = require('del');
    environments = require('gulp-environments');

var development = environments.development;
var production = environments.production;

var config = {
    bootstrapDir: './bower_components/bootstrap-sass',
    ngToggleDir: './bower_components/ngToggle',
    select2Dir: './bower_components/select2',
    select2ThemeDir: './bower_components/select2-bootstrap-theme/src',
    publicDir: './static/rideshare_frontend',
    bowerDir: './static/bower_components',
};

gulp.task('clean', function (cb) {
    del([config.publicDir], cb);
    del([config.bowerDir], cb);
});


gulp.task('js', function() {
    return gulp.src([
	'bower_components/messageformat/messageformat.js',
	'bower_components/messageformat/locale/en.js',
	'bower_components/messageformat/locale/fr.js',
	'bower_components/messageformay/locale/es.js',
	'bower_components/jquery/dist/jquery.min.js', // non-minified version barfs
	'bower_components/twitter-bootstrap-wizard/bootstrap/js/bootstrap.js',
	'bower_components/angular/angular.js',
	'bower_components/angular-animate/angular-animate.js',
	'bower_components/angular-bootstrap-checkbox/angular-bootstrap-checkbox.js',
	'bower_components/angular-cookies/angular-cookies.js',
	'bower_components/angular-resource/angular-resource.js',
	'bower_components/angular-i18n/angular-locale_en.js',

	'bower_components/moment/moment.js',
	'bower_components/moment/locale/fr.js',
	'bower_components/moment/locale/es.js',
	'bower_components/angular-moment/angular-moment.js',
	'bower_components/angular-sanitize/angular-sanitize.js',
	'bower_components/angular-ui-router/release/angular-ui-router.js',
	'bower_components/angular-gravatar/build/angular-gravatar.js',
	'bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
	'bower_components/angular-touch/angular-touch.js',
	'bower_components/angular-translate/angular-translate.js',

	'bower_components/angular-translate-storage-local/angular-translate-storage-local.js',
	'bower_components/angular-translate-storage-cookie/angular-translate-storage-cookie.js',
	'bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files.js',
	'bower_components/venturocket-angular-slider/build/angular-slider.js',
	'bower_components/angular-ui-utils/ui-utils.js',
	'bower_components/angular-preloaded/build/angular-preloaded.js',
	'bower_components/angular-translate-interpolation-messageformat/angular-translate-interpolation-messageformat.js',
	'bower_components/intl-tel-input/build/js/intlTelInput.js',
	'bower_components/international-phone-number/releases/international-phone-number.js',
	'bower_components/angular-dynamic-locale/src/tmhDynamicLocale.js',
	'bower_components/ngToggle/ng-toggle.js',
	'bower_components/angularjs-utilities/lib/jquery.bootstrap.wizard.js',
	'bower_components/angularjs-utilities/src/directives/rcSubmit.js',
	'bower_components/angularjs-utilities/src/modules/rcForm.js',
	'bower_components/angularjs-utilities/src/modules/rcDisabled.js',
	'bower_components/angularjs-utilities/src/modules/rcWizard.js',
	'bower_components/resource_patch.js',

	'./src/app/**/*.js'])
       .pipe(concat('rideshare.js'))
       .pipe(production(uglify()))
       .pipe(gulp.dest(config.publicDir + '/js'));
});

gulp.task('sass', function() {
    return gulp.src(['./src/sass/rideshare.scss', './src/sass/directory.scss'])
    .pipe(sass({
        includePaths: [
	  config.bootstrapDir + '/assets/stylesheets',
	  config.ngToggleDir,
	  config.select2Dir,
	  config.select2ThemeDir
	  ],
    }))
    .pipe(production(cleanCSS()))
    .pipe(gulp.dest(config.publicDir + '/css'));
});

gulp.task('fonts', function() {
    return gulp.src(config.bootstrapDir + '/assets/fonts/**/*')
    .pipe(gulp.dest(config.publicDir + '/fonts'));
});

// Some of the bower_components need to be accessible at run time, and can't be compiled
// into the rideshare.js file.
// Also, simply copying over some of the fonts and image files.
gulp.task('copy', function() {
     gulp.src([
	'bower_components/angular-i18n/**/*.js',
	'bower_components/intl-tel-input/**/*.js',
         './src/font-awesome-4.2.0/**/*',
         './src/fonts/**/*',
         './src/img/**/*'], {base:"./src"})
         .pipe(gulp.dest(config.publicDir))
         //.pipe(browserSync.stream())
});

// This gets the templates in views and partials
gulp.task('html', function() {
     gulp.src(['./src/**/*.html'], {base:"./src"})
         .pipe(production(htmlmin({'collapseWhitespace':true})))
         .pipe(gulp.dest(config.publicDir))
         //.pipe(browserSync.stream())
});

gulp.task('default', ['sass', 'fonts', 'js', 'html', 'copy']);
