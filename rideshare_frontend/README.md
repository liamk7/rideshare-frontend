# Rideshare Frontend

## Build System

The build system for rideshare_frontend uses [npm](https://www.npmjs.com/),
[bower](https://bower.io/), [gulp](http://gulpjs.com/) and [sass](http://sass-lang.com/).

## Installation


1. You *do* need [the latest](https://tecadmin.net/install-latest-nodejs-npm-on-ubuntu/#) nodejs/npm.
   >**NOTE**: must be 11.x.  Version 12 doesn't work with gulp v.3
                                                                                                       
   `curl -sL https://deb.nodesource.com/setup_11.x | sudo bash -`
1. `sudo npm install gulp -g`
1. `npm install`

### gulp

* Run `gulp --tasks` to list the tasks
* Run `gulp` to run all the tasks (development mode)
* Run `gulp --env production` to run all the tasks (production mode)
* Run `gulp clean` to delete the built files

In production mode, the CSS and JS files are minified, and the HTML files are compressed.

**gulp** copies the built files into the `static/rideshare_frontend` directory, which is where Django looks for them by default.

### sass

See the [README](./src/sass/README.md) file in the sass directory.
