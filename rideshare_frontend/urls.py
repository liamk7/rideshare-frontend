from django.conf import settings
from django.conf.urls import url
from .views import rideshare_root as rr

# Normally, the links below are routed locally on the client with AngularJS.
# However, we also allow them to pass through here so that a page reload
# ends up with the same page.
# Allow invalid urls for /event/slug/detail and /event/slug/edit and
# /event/

urlpatterns = [
    url(r'^$', rr, name="rideshare_home"),
    url(r'^index.html$', rr, name="rideshare_index"),
    url(r'^help$', rr, name="rideshare_help"),
    url(r'^faq$', rr, name="rideshare_faq"),
    url(r'^event/(?P<slug>[\w\d-]+)/search/?$', rr, name="rideshare_search"),
    # for these that require an object_id, allow missing value to pass through
    # so that it's handled by Angular in resolve. That is, \d* instead of \d+.
    url(r'^event/(?P<slug>[\w\d-]+)/detail/(?P<object_id>\d*)/?$',
        rr, name="rideshare_detail"),
    url(r'^event/(?P<slug>[\w\d-]+)/edit/(?P<object_id>\d*)/?$',
        rr, name="rideshare_edit"),
    url(r'^event/(?P<slug>[\w\d-]+)/create/?$', rr, name="rideshare_create"),
    url(r'^event/(?P<slug>[\w\d-]+)/?$', rr, name="rideshare_event"),
    url(r'^event/(?P<event_base>[\w\d-]+)-(?P<year>\d{4})/?$', rr, name="rideshare_event_default"), # for directory

    # event catch-all
    url(r'^event/.*', rr, name="rideshare_event_catchall"),

    url(r'^error/?$', rr, name="rideshare_error"),
]
