import json
from django.conf import settings
from django.shortcuts import render

def rideshare_root(request, **kwargs):
    """ This is the main entry point for the AngularJS application.
        It inserts some custom preload JavaScript needed by Angular.
    """
    # Set these in the settings.py or local_settings.py file.
    region_slug = getattr(settings, 'RIDESHARE_REGION_SLUG')
    kiosk_mode = getattr(settings, 'KIOSK_MODE', False)

    # The angular.html template delivers the angular application and
    # associated js and css files.  This can be overridden in a project
    # that imports rideshare by creating a template with the same name.
    template_name = 'rideshare_frontend/angular.html'

    # You can send the whole region here, and potentially avoid a second
    # API call to retrieve it.  However, just setting the region_slug will
    # probably be easier for Regional deployments, and it's a quick call.
    # Also, we are injecting the opensearch results here, if available.
    #serializer = RegionalNodeSerializer(region)
    #preload_json = JSONRenderer().render(serializer.data)
    preload_data = { 'region_slug':region_slug,
                     'kiosk': kiosk_mode,
                     }
    preload_json = json.dumps(preload_data)
    context = {
        'preload_json': preload_json,
    }
    return render(
        request,
        template_name,
        context,
    )
