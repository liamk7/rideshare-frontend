Customizing Rideshare Frontend SASS
===================================

> **NOTE**: These files *also* create the CSS files for the "directory" project.

There are several places to go about customizing the rideshare_frontend application.

The *_variables.scss files are copies of the Bootstrap configuration file that you 
can customize.  Mostly this is useful for setting colors, which then are used by
bootstrap to make them consistent throughout the application.
In addition, of course, you can use any of the variables set in those files in 
the subsequent files that you include.

The *_app.scss files are per-application overrides and additions.

Generally, try to use existing Bootstrap classes in the HTML 
and customize them or override them here.

The approach is modeled after [this](https://www.codementor.io/trey/create-custom-bootstrap-build-with-scss-du107p62v).

These are specific to the "directory" project

* directory.scss
* _directory_variables.scss
* _directory_app.scss

These are specific to the main Burning Man implementation

* rideshare.scss
* _rideshare_variables.scss
* _rideshare_app.scss

These are shared with all implementations

<table>
<tr><td width="25%">_base.scss</td><td>Basic stuff</td>
<tr><td>_rc-wizard.scss	</td><td>Tweaking the wizard</td>
<tr><td>_form_buttons.scss</td><td>Special form buttons for the wizard, or elsewhere</td>
<tr><td>_slider.scss</td><td>Stuff specific to the slider directive on the search page</td>
</table>

After changing any of these files, 

1. Run `gulp sass`
1. Run `python manage.py collectstatic` (when not in debug mode)
