'use strict';

// Declare app level module which depends on filters, and services
var app = angular.module('rideshares',
    [ 'ui.gravatar', 'ui.checkbox', 'rcWizard', 'rcForm', 'rcDisabledBootstrap', 'ui.router', 'ngAnimate', 'ngCookies',
      'ngSanitize', 'vr.directives.slider', 'rideshares.filters', 'rideshares.services', 'rideshares.directives',
      'rideshares.controllers', 'rideshares.providers', 'ui.bootstrap', 'ngToggle', 'gs.preloaded',
      'pascalprecht.translate', 'angularMoment', 'internationalPhoneNumber', 'tmh.dynamicLocale']);

app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$preloaded', '$translateProvider',
            '$compileProvider', '$resourceProvider', '$httpProvider', 'uibDatepickerConfig', 'uibDatepickerPopupConfig',
	    'tmhDynamicLocaleProvider', 'gravatarServiceProvider', 'EnvironmentProvider',
            function($stateProvider, $urlRouterProvider, $locationProvider, $preloaded, $translateProvider,
            $compileProvider, $resourceProvider, $httpProvider, uibDatepickerConfig, uibDatepickerPopupConfig, tmhDynamicLocaleProvider,
            gravatarServiceProvider, EnvironmentProvider) {

    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    $httpProvider.defaults.withCredentials = true;

    // Various different environments for debugging and production
    EnvironmentProvider.setEnvironments({
        dev: {
            root: 'http://dev.rideshare.burningman.org',
            api: '/api',
            port: 80,
            version: ''
        },
        debug: {
            root: 'http://localhost',
            port: 8000,
            api: '/api',
            version: ''
        },
        localonly: {
            root: 'http://localhost',
            port: 80,
            api: '/api',
            version: ''
        },
        prod: {
            root: 'https://rideshare.burningman.org',
            port: 443,
            api: '/api',
            version: ''
        }
    });

    //Set prod as the active schema for production
    EnvironmentProvider.setActive('prod');

    $resourceProvider.defaults.stripTrailingSlashes = false;

    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false,
        rewriteLinks: false
     }).hashPrefix('!');

    // Disable debug data for production
    $compileProvider.debugInfoEnabled(false);

    // dynamicLocale options
    tmhDynamicLocaleProvider.localeLocationPattern("/static/bower_components/angular-i18n/angular-locale_{{locale}}.js");

    // Some defaults for the ui.bootstrap.datepicker can be set here
    // Popup settings
    uibDatepickerPopupConfig.datepickerPopup = 'fullDate';
    uibDatepickerPopupConfig.showButtonBar = false;
    uibDatepickerPopupConfig.closeOnDateSelection = true;
    // Datepicker Settings
    uibDatepickerConfig.showWeeks = false;
    uibDatepickerConfig.yearRange = 2;

    // Translation setup
    $translateProvider.translations('en', {
        // Navbar links
        CHOOSE_AN_EVENT: "Choose an Event",
        HOME: "Home",
        SEARCH: "Search Listings",
        CREATE: "Create a Listing",
        SETTINGS: "Settings",
        MILES: "Miles",
        KILOMETERS: "Kilometers",
        LANGUAGES: "Languages",
        FAQ: "FAQ",
        HELP: "Help",
        BUTTON_LANG_EN: "English",
        BUTTON_LANG_DE: "German",
        BUTTON_LANG_FR: "French",
        BUTTON_LANG_ES: "Spanish",

        // Modal buttons
        BACK: "Back",
        CONTINUE: "Continue",
        CANCEL: "Cancel",
        SEND: "Send",
        SUBMIT: "Submit",
        NEXT: "Next",
        PREVIOUS: "Previous",
        FIRST: "First",
        LAST: "Last",
        MODAL_REQUIRED: "Required",
        REQUIRED: "",
        VALID_EMAIL_REQUIRED: "Valid email required",

        // Modal messages
        DELETE_MSG_HEADER: "You are about to permanently delete",
        FILLED_MSG_HEADER: "You've found someone to go with!",
        FILLED_MSG_TEXT: "Marking your listing as <q lang=\"en\">filled</q> let's us know we were successful in helping you find a ride or passenger. It will remove your listing from the location search listing. However, you will still be able to find it by searching for it by name.",
        UNFILLED_MSG_HEADER: "Mark your listing as active",
        UNFILLED_MSG_TEXT: "Marking your entry as <q lang=\"en\">active</q> will make it visible in search results.",

        // Event home page
        WELCOME_TO: "Welcome to",
        WELCOME_MSG: "<p><strong>This is where you can request a ride from, or offer a ride to other folks headed to (or from) Black Rock City and other Burning Man related events.</strong></p>" +
            "<p>Choose the event you're going to, and create a Rideshare listing. You can also search all existing listings to find a ride (or passenger) that works for you!</p>",
        NO_EVENTS: "There are currently no events scheduled.",
        GOING_TO: "Going to",
        BEGINS_AND_ENDS: "It begins {{ start_date | amDateFormat:'LL' }} and ends {{ end_date | amDateFormat:'LL' }}.",
        ITS_IN: "It's in {{ location }}.",
        GO_TO_SITE: "Visit site",

        // Activity table
        ACTIVITY: "Activity",
        OFFERS: "Offers",
        REQUESTS: "Requests",
        FILLED: "Filled",
        TO_EVENT: "to event",
        FROM_EVENT: "from event",

        // Search page, searchForm filter values, item fields
        IM_GOING: "I'm going",
        TO: "to",
        FROM: "from",
        FILTER: "Filter",
        REMOVE: "Remove",
        CLOSE: "Close",
        MY_LOCATION: "My Location",
        MY_DESTINATION: "My destination",
        SEARCH_BY_NAME: "Search by Name",
        RIDE_TYPE: "Listing Type",
        RIDE_TYPE_TIP: "You are offering or requesting a ride.",
        DIRECTION: "Direction",
        ITINERARY: "Itinerary",
        HAULING: "Hauling",
        HAULING_TIP: "Unusual size or quantity of luggage.  Describe in detail in next step.",
        FLIGHT: "Flight",
        VEHICLE: "Vehicle",
        PASSENGERS: "Passengers",
        PASSENGERS_SEARCH_TIP: "Selecting Yes and a number of passengers, in combination with Offer or Request, will display listings with corresponding minimum or maximum number of passengers",
        PASSENGERS_CREATE_TIP: "Are passenger seats requested or offered?  If so, how many? If just hauling, select No.",
        HOW_MANY: "How Many?",
        PLURAL_PASSENGERS: "{COUNT, plural, one{# passenger} other{# passengers}}",
        ANY: "Any",
        OFFER: "Offer",
        REQUEST: "Request",
        YES: "Yes",
        NO: "No",
        ONE_WAY: "One Way",
        ROUND_TRIP: "Round Trip",
        LOADING: "Loading",
        UNKNOWN_LOCATION: "unknown location",
        SEARCH_RADIUS: "Search radius",
        PAGE: "Page",
        RESET_FILTER: "Reset Filter",
        NO_RESULTS: "No results",
        SEARCH_RESULTS_ALL_EVENTS: "Search Results For All Events",


        // Detail page, including edit functions
        LOGIN: "Log in",
        LOGOUT: "Log out",
        DELETE: "Delete",
        EDIT: "Edit",
        CHECK_MESSAGES: "Check Messages",
        DIRECTIONS: "directions",
        CREATED: "Created",
//        RIDE: "ride",
        DEPART: "Depart",
        RETURN: "Return",
        DESCRIPTION: "Description",
        CONTACT: "Contact",
        NEED_TRANSLATION: 'Do you need a <a href="https://translate.google.com/" target="_blank">translation</a>?',
        PLURAL_MESSAGES: "You have {COUNT, plural, =0{no messages} =1{a message!} other{# messages}}",
        BAGGAGE_WEIGHT: "Max Baggage Weight",
        BAGGAGE_WEIGHT_PLACEHOLDER: "Baggage Weight",
        BAGGAGE_WEIGHT_TIP: "Maximum allowed baggage weight",
        PUBLIC_CONTACT_INFO: "Event contact info",
        PUBLIC_CONTACT_INFO_PLACEHOLDER: "Contact me at the event by…",
        IMPORTANT_FLIGHT_INFORMATION: "Important flight information",
        SMS_TIP: "Receive text messages when someone responds to your listing", //Fix French/Spanish
        EDIT_RIDESHARE: "Edit Listing",
        IS_THIS_YOUR_POST: "Is this your post?",
        NO_PASSENGERS: "None", // as in "Passengers: None"

        // Preferences (detail page, create page)
        PREFERENCES: "Preferences",
        SMOKING: "Smoking",
        NON_SMOKING: "Non-smoking",
        DISABLED: "Accommodate Disabled",
        OPEN_MINDED: "Open-minded",

        // Authentication
        HINT: "Hint",
        PASSWORD: "Password",
        VERIFY_PASSWORD: "Re-type password!",
        PASSWORD_HINT: "Password hint",
        PASSWORD_PLACEHOLDER: "My password",
        PASSWORD_VERIFY_PLACEHOLDER: "Verify password",
        PASSWORD_HINT_PLACEHOLDER: "Password hint",
        USERNAME: "Username",
        INVALID_LOGIN: "Invalid login… please try again.",

        // Contact form
        NAME: "Name",
        EMAIL: "Email",
        PHONE: "Phone",
        CONTACT_INFO: "Best way to contact me at the event",
        MY_NAME: "My Name",
        MY_EMAIL: "My Email",
        MY_PHONE: "My Phone Number",
        CONTACT_INFO_PLACEHOLDER: "The best way to contact me at the event is…",
        MESSAGE: "Message",

        // Help Comment Form
        SUBJECT: "Subject",
        MY_SUBJECT: "Subject",

        // Modal Feedback forms
        SUCCESS: "Success",
        FAILURE: "Error",
        SENDING: "Sending…",
        SEND_SUCCESS_MSG: "Your message was sent",
        SEND_FAILURE_MSG: "There was an error sending your message",
        LISTING_SUCCESS_MSG: "Your listing has been {OPERATION, select, create{created} update{updated} delete{deleted} other{accessed} }.",
        GENERAL_FAILURE_MSG: "We were unable to complete your request.  Please try again later.",

        // Create/Edit form translation keys not already listed
        LISTING_TITLE: "Listing Title",
        LISTING_TITLE_PLACEHOLDER: "My Listing Title",
        CITY: "City",
        AIRPORT: "Airport",
        DETAILS: "Details",
        DESCRIPTION_PLACEHOLDER: "What's important to know? What can you contribute?\nHow much stuff (or room for stuff) do you have?",
        SEND_A_MESSAGE: "Send a message",
        GRAVATAR_TIP: "An <q lang=\"en\">avatar</q> is an image that represents you online &ndash; a little picture that appears next to your name when you interact with websites.  " +
            "A Gravatar is a Globally Recognized Avatar. You upload it and create your profile just once, and then when you participate in any Gravatar-enabled site, " +
            "your Gravatar image will automatically follow you there.",
        GRAVATAR_CHANGE: "Create or Change your Gravatar",
        GRAVATAR_OPTIONAL: "Optional.  This is a third party site",
        PASSWORDS_MUST_MATCH: "Passwords must match",
        PASSWORD_TIP: "This password allows you to update your listing, and access messages you receive. See FAQs for more information about authentication.",
        LISTING_EXPIRED: "This listing is for an event in the past.  It cannot be changed.",
        EVENT_EXPIRED: "You cannot create a listing for an event in the past.",
        ERROR_MSG: "Something seems to have gone wrong.",
        NOT_FOUND: "We were unable to find the resource you requested.",
        UNAUTHORIZED: "You must authenticate in order to use this resource."


    });

    $translateProvider.translations('fr', {
        // Navbar links
        CHOOSE_AN_EVENT: "Choisir un événement",
        HOME: "Acceuil",
        SEARCH: "Rechercher Entrées",
        CREATE: "Créer Entrée",
        SETTINGS: "Paramètres",
        MILES: "Miles",
        KILOMETERS: "Kilomètres",
        LANGUAGES: "Langues",
        FAQ: "FAQ",
        HELP: "Aide",
        BUTTON_LANG_EN: "Anglais",
        BUTTON_LANG_DE: "Allemand",
        BUTTON_LANG_FR: "Français",
        BUTTON_LANG_ES: "Espagnol",

        // Modal buttons
        BACK: "Retour",
        CONTINUE: "Continuer",
        CANCEL: "Annuler",
        SEND: "Envoyer",
        SUBMIT: "Soumettre",
        NEXT: "Suivant",
        PREVIOUS: "Précédent",
        FIRST: "Premier",
        LAST: "Dernier",
        MODAL_REQUIRED: "Obligatoire",
        REQUIRED: "",

        VALID_EMAIL_REQUIRED: "Email valide requiré",

        // Modal messages
        DELETE_MSG_HEADER: "Vous êtes au point de supprimer définitivement",
        FILLED_MSG_HEADER: "Vous avez trouvé quelqu'un!",
        FILLED_MSG_TEXT: "Valider votre entrée en tant que <q lang=\"fr\">rempli</q> nous permet de savoir que nous avons réussi à vous aider à trouver un covoiturage ou un passager.  Il sera retiré de la liste de recherche.  Toutefois, vous pouvez toujours le trouver en le cherchant par nom.",
        UNFILLED_MSG_HEADER: "Valider votre entrée active",
        UNFILLED_MSG_TEXT: "Valider votre entrée en tant que <q lang=\"fr\">active</q> le fait visible dans la liste de recherche.",

        // Home page and Event home page
        WELCOME_TO: "Bienvenue à",
        WELCOME_MSG: "<p><strong>Voilà l'endroit pour rechercher, ou offrir, un covoiturage à la communauté de brûleurs.</strong></p>" +
            "<p>Choisissez votre événement.  Vous pouvez rechercher toutes les annonces, et trouver votre nouveau meilleur copain de burn.</p>",
        NO_EVENTS: "Actuellment, il n'y a aucun événement prévu.",
        GOING_TO: "Allez-vous à",
        BEGINS_AND_ENDS: "Il commence à {{ start_date | amDateFormat:'LL' }} et finit {{ end_date | amDateFormat:'LL' }}.",
        ITS_IN: "Il se déroule à {{ location }}.",
        GO_TO_SITE: "Aller au site",


        // Aggregate table (changed to singular…)
        ACTIVITY: "Activité",
        OFFERS: "Offre",
        REQUESTS: "Requête",
        FILLED: "Rempli",
        TO_EVENT: "à l'événement",
        FROM_EVENT: "de l'événement",

        // Search page, searchForm filter values, item fields
        IM_GOING: "Je vais",
        TO: "à",
        FROM: "de",
        FILTER: "Filtre",
        REMOVE: "Supprimer",
        CLOSE: "Fermer",
        MY_LOCATION: "Mon emplacement",
        MY_DESTINATION: "Ma destination",
        SEARCH_BY_NAME: "Rechercher par nom",
        RIDE_TYPE: "Type",
        RIDE_TYPE_TIP: "Un offre ou une requête",
        DIRECTION: "Sens",
        ITINERARY: "Itinéraire",
        HAULING: "Transport",
        HAULING_TIP: "Quantité ou taille inhabituelle de bagage",
        FLIGHT: "Vol",
        VEHICLE: "Véhicule",
        PASSENGERS: "Passagers",
        PASSENGERS_SEARCH_TIP: "Vous amenez passagers… combien?",
        PASSENGERS_CREATE_TIP: "Si vous demandez ou offrez un covoiturage pour les passagers, et si oui, combien",
        NO_RESULTS: "Aucun résultat",
        SEARCH_RESULTS_ALL_EVENTS: "Résultats Pour Tous Les Événments",

        HOW_MANY: "Combien?",
        PLURAL_PASSENGERS: "{COUNT, plural, one{# passager} other{# passagers}}",
        ANY: "Tout",
        OFFER: "Offre",
        REQUEST: "Requête",
        YES: "Oui",
        NO: "Non",
        ONE_WAY: "Aller simple",
        ROUND_TRIP: "Aller-retour",
        LOADING: "Chargement",
        UNKNOWN_LOCATION: "endroit inconnu",
        SEARCH_RADIUS: "Radius de recherche",
        PAGE: "Page",
        RESET_FILTER: "Remettre Filtre",

        // Detail page, including edit functions
        LOGIN: "Connexion",
        LOGOUT: "Déconnexion",
        DELETE: "Supprimer",
        EDIT: "Éditer",
        CHECK_MESSAGES: "Messages",
        CREATED: "Créé",
//        RIDE: "covoiturage",
        DEPART: "Départ",
        RETURN: "Retour",
        DESCRIPTION: "Description",
        CONTACT: "Contacter",
        DIRECTIONS: "itinéraire",
        NEED_TRANSLATION: 'Avez-vous besoin d\'une <a href="https://translate.google.com/", target="_blank">traduction</a>?',
        PLURAL_MESSAGES: "{COUNT, plural, =0{Vous n'avez aucun message} =1{Vous avez un message!} other{Vous avez # messages}}",
        BAGGAGE_WEIGHT: "Max Poids de Bagages",
        BAGGAGE_WEIGHT_PLACEHOLDER: "Poids de Bagages",
        BAGGAGE_WEIGHT_TIP: "Maximum poids de bagages",
        PUBLIC_CONTACT_INFO: "Coordonnées à l'événement",
        PUBLIC_CONTACT_INFO_PLACEHOLDER: "Le meilleur façon de me contacter à l'événement est…",
        IMPORTANT_FLIGHT_INFORMATION: "Informations importantes de vol",
        SMS_TIP: "Recevoir un message texte lorsque quelqu'un vous contacte",
        EDIT_RIDESHARE: "Éditer Entŕee",
        IS_THIS_YOUR_POST: "Est-ce votre poste?",
        NO_PASSENGERS: "Aucun", // as in "Passengers: None"



        // Preferences (detail page, create page)
        PREFERENCES: "Préférences",
        SMOKING: "Fumer",
        NON_SMOKING: "Ne pas fumer",
        DISABLED: "Personnes à mobilité réduite",
        OPEN_MINDED: "Ouvert d'ésprit",

        // Authentication
        HINT: "Indice",
        PASSWORD: "Mot de passe",
        VERIFY_PASSWORD: "Re-taper le mot de passe!",
        PASSWORD_HINT: "Indice de mot de passe",
        PASSWORD_PLACEHOLDER: "Mon mot de passe",
        PASSWORD_VERIFY_PLACEHOLDER: "Verifier mot de passe",
        PASSWORD_HINT_PLACEHOLDER: "Indice de mot de passe",
        USERNAME: "Nom d'utilisateur",
        INVALID_LOGIN: "Connexion invalide… s'il vous plaît essayer à nouveau.",

        // Contact form
        NAME: "Nom",
        EMAIL: "Email",
        PHONE: "Téléphone",
        CONTACT_INFO: "Meilleur façon de me contacter a l'évènement",
        MY_NAME: "Mon nom",
        MY_EMAIL: "Mon email",
        MY_PHONE: "Mon Numéro de Téléphone",
        CONTACT_INFO_PLACEHOLDER: "Le meilleur façon de me contacter a l'évènement…",
        MESSAGE: "Message",

        // Help Comment Form
        SUBJECT: "Sujet",
        MY_SUBJECT: "Sujet",

        // Modal Feedback forms
        SUCCESS: "Succès",
        FAILURE: "Échec",
        SENDING: "Envoi en cours…",
        SEND_SUCCESS_MSG: "Votre message a été envoyé",
        SEND_FAILURE_MSG: "Il y avait une erreur d'envoyer votre message",
        LISTING_SUCCESS_MSG: "Votre entrée a été {OPERATION, select, create{crée} update{mis à jour} delete{supprimé} other{accessée} }.",
        GENERAL_FAILURE_MSG: "On n'a pas pu accomplir votre requête.  S'il vous plaît essayer à nouveau plus tard.",

        // Create/Edit form translation keys not already listed
        LISTING_TITLE: "Titre d'Entrée",
        LISTING_TITLE_PLACEHOLDER: "Mon entrée",
        CITY: "Ville",
        AIRPORT: "Aéroport",
        DETAILS: "Détails",
        DESCRIPTION_PLACEHOLDER: "Important de savoir…",
        SEND_A_MESSAGE: "Envoyer un message",
        GRAVATAR_TIP: "Un <q lang=\"fr\">avatar</q> est une image que vous représent en ligne &ndash; une petite image qu'apparaît à coté de votre nomme quand vous rendez visite sites web. a little picture that appears next to your name when you interact with websites.  " +
            "Un Gravatar est un avatar reconnu mondialment. Vous la téléchargez et créez votre profil juste une fois, et puis quand vous participez à un site Gravatar-optimisé, votre image Gravatar vous suivra là automatiquement.",
        GRAVATAR_CHANGE: "Creer ou modifier votre Gravatar",
        GRAVATAR_OPTIONAL: "Optionnel.  Celui-ci est un site d'une tierce partie",
        PASSWORDS_MUST_MATCH: "Mots de passe ne correspondent pas",
        PASSWORD_TIP: "Ce mot de passe vous permet de mettre à jour votre entrée, et d'accéder à les message que vous recevez.  Consultez les FAQs pour en savoir plus sur l'authentification.",
        LISTING_EXPIRED: "Cette entrée est pour un événement dans le passé.  Il ne peut pas être changé.",
        EVENT_EXPIRED: "Vous ne pouvez pas créer une entrée pour un événement dans le passé.",
        ERROR_MSG: "Quelque chose semble avoir mal tourné.",
        NOT_FOUND: "Nous avons été incapables de trouver la ressource que vous avez demandé.",
        UNAUTHORIZED: "Vous devez vous authentifier pour pouvoir utiliser cette ressource."


    });
    $translateProvider.translations('es', {
        // Navbar links
        CHOOSE_AN_EVENT: "Elije un Evento",
        HOME: "Inicio",
        SEARCH: "Buscar Entradas",
        CREATE: "Crea una Entrada",
        SETTINGS: "Configuración",
        MILES: "Millas",
        KILOMETERS: "Kilómetros",
        LANGUAGES: "Idiomas",
        FAQ: "FAQ",
        HELP: "Ayuda",
        BUTTON_LANG_EN: 'Inglés',
        BUTTON_LANG_DE: 'Alemán',
        BUTTON_LANG_FR: 'Francés',
        BUTTON_LANG_ES: "Español",

        // Modal buttons
        BACK: "Atrás",
        CONTINUE: "Continuar",
        CANCEL: "Anular",
        SEND: "Enviar",
        SUBMIT: "Presentar",
        NEXT: "Siguiente",
        PREVIOUS: "Anterior",
        FIRST: "Primero",
        LAST: "Último",
        MODAL_REQUIRED: "Obligatorio",
        REQUIRED: "",
        VALID_EMAIL_REQUIRED: "Email válido necesario",

        // Modal messages
        DELETE_MSG_HEADER: "Estás a punto de eliminar permanentamente",
        FILLED_MSG_HEADER: "¡Has encontrado a alguien para ir!",
        FILLED_MSG_TEXT: "Marcando su entrada como <q lang=\"es\">completa</q> Dinos si tuvimos éxito en ayudarle a organizar el viaje. Su entrada será eliminada de la lista de búsqueda general. Pero tú podrás encontrarla en buscandola por tu nombre.",
        UNFILLED_MSG_HEADER: "Marcar entrada como activa",
        UNFILLED_MSG_TEXT: "Marcando una entrada como <q lang=\"es\">activa</q> la hace visible en la lista de búsqueda.",

        // Home page & Event home page
        WELCOME_TO: "Bienvenidos a",
        WELCOME_MSG: "<p><strong>Este es el lugar para pedir, u ofrecer, un viaje a la comunidad de burners.</strong></p>" +
            "<p>Elije tu evento.  Tu puedes buscar todas las entradas, y encontrar tu nuevo mejor camarada de burn.</p>",
        NO_EVENTS: "Actualmente, no hay ningún evento previsto.",
        GOING_TO: "Ir a",
        BEGINS_AND_ENDS: "Desde el {{ start_date | amDateFormat:'LL' }} hasta el  {{ end_date | amDateFormat:'LL' }}.",
        ITS_IN: "Está en {{ location }}.",
        GO_TO_SITE: "Ir al sitio",


        // Activity table
        ACTIVITY: "Actividad",
        OFFERS: "Ofertas",
        REQUESTS: "Solicitudes",
        FILLED: "Completo",
        TO_EVENT: "al evento",
        FROM_EVENT: "del evento",

        // Search page, searchForm filter values, item fields
        IM_GOING: "Voy",
        TO: "a",
        FROM: "de",
        FILTER: "Filtro",
        REMOVE: "Eliminar",
        CLOSE: "Cerrar",
        MY_LOCATION: "Mi ubicación",
        MY_DESTINATION: "Mi destino",
        SEARCH_BY_NAME: "Buscar por nombre",
        RIDE_TYPE: "Tipo de Entrada",
        RIDE_TYPE_TIP: "Oferta o solicitud",
        DIRECTION: "Dirección",
        ITINERARY: "Itinerario",
        HAULING: "Transporte de carga",
        HAULING_TIP: "Cantidad o tamaño de equipaje de forma inusual",
        FLIGHT: "Vuelo",
        VEHICLE: "Vehículo",
        PASSENGERS: "Pasajeros",
        PASSENGERS_SEARCH_TIP: "¿Cuántos pasajeros?",
        PASSENGERS_CREATE_TIP: "Si estás solicitando u ofreciendo un viaje para pasajeros, y si es así, ¿cuántos?",
        NO_RESULTS: "Ningun resultado",
        SEARCH_RESULTS_ALL_EVENTS: "Resultados Para Todos Los Eventos",

        HOW_MANY: "¿Cuántos?",
        PLURAL_PASSENGERS: "{COUNT, plural, one{# pasajero} other{# pasajeros}}",
        ANY: "Algún",
        OFFER: "Oferta",
        REQUEST: "Solicitud",
        YES: "Sí",
        NO: "No",
        ONE_WAY: "Solo ida",
        ROUND_TRIP: "Ida y vuelta",
        LOADING: "Cargando",
        UNKNOWN_LOCATION: "Ubicación desconocida",
        SEARCH_RADIUS: "Radio de busca",
        PAGE: "Página",
        RESET_FILTER: "Limpiar filtros",

        // Detail page, including edit functions
        LOGIN: "Iniciar sesión",
        LOGOUT: "Cerrar sesión",
        DELETE: "Eliminar",
        EDIT: "Editar",
        CHECK_MESSAGES: "Leer mensajes",
        DIRECTIONS: "indicaciones",
        CREATED: "Creado",
//        RIDE: "viaje",
        DEPART: "Partir",
        RETURN: "Vuelta",
        DESCRIPTION: "Descripción",
        CONTACT: "Contactar",
        NEED_TRANSLATION: '¿Necesitas una <a href="https://translate.google.com/", target="_blank">traducción</a>?',
        PLURAL_MESSAGES: "Tienes {COUNT, plural, =0{ningún mensaje} =1{uno mensaje} other{# mensajes}}",
        BAGGAGE_WEIGHT: "Máx Peso de Equipaje",
        BAGGAGE_WEIGHT_PLACEHOLDER: "Peso de equipaje",
        BAGGAGE_WEIGHT_TIP: "Máximo peso de equipaje permitido",
        PUBLIC_CONTACT_INFO: "Contactar al evento",
        PUBLIC_CONTACT_INFO_PLACEHOLDER: "El mejor modo de contactarme al evento es…",
        IMPORTANT_FLIGHT_INFORMATION: "Información de vuelo importante",
        SMS_TIP: "Recibir mensajes de texto cuando alguien me contacte.",
        EDIT_RIDESHARE: "Editar Entrada",
        IS_THIS_YOUR_POST: "¿Esta es tu mensaje?",
        NO_PASSENGERS: "Ningun", // as in "Passengers: None"



        // Preferences (detail page, create page)
        PREFERENCES: "Preferencias",
        SMOKING: "Fumar",
        NON_SMOKING: "No fumar",
        DISABLED: "Equipada para minusválidos",
        OPEN_MINDED: "Mente abierta",

        // Authentication
        HINT: "Pista",
        PASSWORD: "Contraseña",
        VERIFY_PASSWORD: "¡Confirma contraseña!",
        PASSWORD_HINT: "Pista de contraseña",
        PASSWORD_PLACEHOLDER: "Mi contraseña",
        PASSWORD_VERIFY_PLACEHOLDER: "Confirma contraseña",
        PASSWORD_HINT_PLACEHOLDER: "Pista de contraseña",

        USERNAME: "Usuario",
        INVALID_LOGIN: "Login inválido… Inténtalo otra vez, por favor.",

        // Contact form
        NAME: "Nombre",
        EMAIL: "Email",
        PHONE: "Teléfono",
        CONTACT_INFO: "Mejor modo de contactarme al evento",
        MY_NAME: "Mi nombre",
        MY_EMAIL: "Mi email",
        MY_PHONE: "Mi número de teléfono",
        CONTACT_INFO_PLACEHOLDER: "El mejor modo de contactarme al evento es…",
        MESSAGE: "Mensaje",

        // Help Comment Form
        SUBJECT: "Asunto",
        MY_SUBJECT: "Asunto",

        SUCCESS: "Éxito",
        FAILURE: "Error",
        SENDING: "Enviando…",
        SEND_SUCCESS_MSG: "Tu mensaje ha sido enviado",
        SEND_FAILURE_MSG: "Se produjó un error al enviar tu mensaje",
        LISTING_SUCCESS_MSG: "Tu entrada se ha {OPERATION, select, create{creado} update{actualizado} delete{eliminado} other{accessado} }.",
        GENERAL_FAILURE_MSG: "No fuimos capaces de completar tu solicitud.  Inténtalo otra vez, por favor, mas tarde.",

        // Create/Edit form translation keys not already listed
        LISTING_TITLE: "Título de Entrada",
        LISTING_TITLE_PLACEHOLDER: "Mi título",
        CITY: "Ciudad",
        AIRPORT: "Aeropuerto",
        DETAILS: "Detalles",
        DESCRIPTION_PLACEHOLDER: "¿Qué es importante a saber?",
        SEND_A_MESSAGE: "Enviar un mensaje",
        GRAVATAR_TIP: "Un <q lang=\"es\">avatar</q> es un imagen que te representa &ndash; un icon pequeño que apparece junto a tu nombre cuando visitas sítios de web.  " +
            "Un Gravatar es un Avatar Reconnocido Globalmente. Tu lo subes y creas tu perfil sólo una vez, y luego, cuando tu participas en cualquier sitio-Gravatar habilitado, tu imagen Gravatar automáticamente te siga allí.",
        GRAVATAR_CHANGE: "Crear o cambiar tu Gravatar",
        GRAVATAR_OPTIONAL: "Opcional. Se trata de un sitio de terceros",
        PASSWORDS_MUST_MATCH: "Contraseñas no se corresponden",
        PASSWORD_TIP: "Esta contraseña te permite actualizar tu perfil, y mensajes que recibes. Consulta Preguntas frecuentes para obtener más información acerca de la autenticación.",
        LISTING_EXPIRED: "Este entrada es para un evento en el pasado. No se puede cambiar.",
        EVENT_EXPIRED: "No se puede crear una entrada por un evento en el pasado.",
        ERROR_MSG: "Algo parece haber salido mal.",
        NOT_FOUND: "No se pudo encontrar el recurso que has solicitado.",
        UNAUTHORIZED: "Debes autenticarte para poder utilizar este recurso."

    });

    $translateProvider.preferredLanguage('en');
    $translateProvider.registerAvailableLanguageKeys(['en', 'fr', 'es']);
    $translateProvider.determinePreferredLanguage();
    $translateProvider.fallbackLanguage('en');
    $translateProvider.useLocalStorage();
    $translateProvider.useSanitizeValueStrategy('sceParameters');
    //$translateProvider.useSanitizeValueStrategy('sce');
    //$translateProvider.useSanitizeValueStrategy('escape');
    //$translateProvider.useSanitizeValueStrategy('sanitize');
//    $translateProvider.useStaticFilesLoader({
//        prefix: '/static/rideshares/language/locale-',
//        suffix: '.json'
//    });

    // This -- plus, you must add explicit "messageformat" for translations that require it
    $translateProvider.addInterpolation('$translateMessageFormatInterpolation');

    // For any unmatched url, redirect to /
    $urlRouterProvider.otherwise("/error");

    $stateProvider
        .state('app', {
            abstract: true,
            url:'/',
            views: {
                "": {
                    controller: 'HomeController',
                    templateUrl: '/static/rideshare_frontend/views/app.html'
                },
                "header@": {
                    controller: 'RegionHeaderController',
                    templateUrl: '/static/rideshare_frontend/views/region_header.html'
                }

            },
            resolve: {
                environmentResource: 'Environment',
                regionResource: 'Region',
                // $preloaded data is included directly in the HTML template
                // region_slug should always be present.
                region_slug: ["$preloaded", function($preloaded) {
                    return $preloaded.region_slug;
                }],
                kiosk: ["$preloaded", function($preloaded) {
		    // Save the default url creation function (urlFunc) for use in online situations.
		    // Replace the urlFunc with this function, and switch to using a static default url
		    // for offline situations.
		    if ($preloaded.kiosk){
			gravatarServiceProvider.urlFunc = function (opts) {
			    return EnvironmentProvider.getDefaultRoute() + '/static/rideshare_frontend/img/embrace_gravatar.jpg';
			}
		    };
                    return $preloaded.kiosk;
                }],
//                region: ["regionResource", "region_slug", function(regionResource, region_slug) {
//                    return regionResource.get({slug:region_slug}).$promise;
//                }
                region: ["$q", "$state", "regionResource", "region_slug", function($q, $state, regionResource, region_slug) {
                    var deferred = $q.defer();
                    regionResource.get({"slug":region_slug}).$promise.then(
                        function(ev) {
                            return deferred.resolve(ev);
                        },
                        function(error) {
                            // This should never happen.  It would indicate an error with the backend sending
                            // a bad or missing preloaded region slug. Or a failure to deal with JSON
                            // vulnerability protection.
                            console.error('Region resolve failed', error);
                            console.error('Unable to access API:', error.config.url);
                            //$state.go('home');
                        }
                    );
                    return deferred.promise;
                }]
            }
        })
        .state('home', {
            url:'', // inherits from app state
            parent: 'app',
            views: {
                "@": {
                    controller: 'HomeController',
                    templateUrl: '/static/rideshare_frontend/views/home.html'
                }
            }
        })
        .state('sitesearch', {
            url:'sitesearch', // inherits from app state
            parent: 'app',
            views: {
                "@": {
                    controller: 'SiteSearchController',
                    templateUrl: '/static/rideshare_frontend/views/sitesearch.html'
                }
            }
        })
        .state('event', {
            parent: 'home',
            url:'event/:slug',
            views: {
                "@": {
                    controller: 'EventHomeController',
                    templateUrl: '/static/rideshare_frontend/views/event_home.html'
                },
                "header@": {
                    controller: 'EventHeaderController',
                    templateUrl: '/static/rideshare_frontend/views/event_header.html'
                }
            },
            resolve: {
                eventResource: 'Event',
                event: ["$q", "$state", "$stateParams", "eventResource", function($q, $state, $stateParams, eventResource) {
                    // if the event slug is missing, or is not found, then go directly to the home page --  going to the
                    // error page results in a loop because of dependency on event.
                    var deferred = $q.defer();
                    if (!$stateParams.slug) { $state.go('home'); }
                    eventResource.get({"slug":$stateParams.slug}).$promise.then(
                        function(ev) {
                            return deferred.resolve(ev);
                        },
                        function(error) {
                            console.error('Event resolve failed');
                            $state.go('home');
                        }
                    );
                    return deferred.promise;
                }]
            }
        })
        .state('event-error', {
            parent: 'event',
            url:'error',
            views: {
                "@": {
                    controller: 'ErrorController',
                    templateUrl: '/static/rideshare_frontend/views/error.html'
                }
            }
        })
        .state('error', {
            parent: 'home',
            url:'error',
            views: {
                "@": {
                    controller: 'ErrorController',
                    templateUrl: '/static/rideshare_frontend/views/error.html'
                }
            }
        })
        .state('search', {
            parent: 'event',
            url:'/search',
            views: {
                "@": {
                    controller: 'SearchController',
                    templateUrl: '/static/rideshare_frontend/views/search.html'
                }
            }
        })
        .state('detail', {
            parent: 'event',
            url:'/detail/:id',
            views: {
                "@": {
                    templateUrl: '/static/rideshare_frontend/views/detail.html',
                    controller: 'DetailController'
                }
            },
            resolve: {
                rideshareResource: 'Rideshare',
                eventResource: 'Event',
                rideshare: ["$q", "$stateParams", "rideshareResource", function($q, $stateParams, rideshareResource) {
                    var deferred = $q.defer();
                    rideshareResource.get({"id":$stateParams.id}).$promise.then(
                        function(rs) {
                            return deferred.resolve(rs);
                        },
                        function(error) {
                            console.error("Rideshare resolve failed in detail");
                            return deferred.reject(error);
                        }
                    );
                    return deferred.promise;
                }]
            }
        })
        .state('create', {
            parent: 'event',
            url:'/create',
            views: {
                "@": {
                    templateUrl: '/static/rideshare_frontend/views/create.html',
                    controller: 'CreateController'
                }
            },
            resolve: {
                rideshare: function() { return null; }
            }
        })
        .state('edit',   {
            parent: 'event',
            url:'/edit/:id',
            views: {
                "@": {
                    templateUrl: '/static/rideshare_frontend/views/create.html',
                    controller: 'CreateController'
                }
            },
            resolve: {
                rideshareResource: 'Rideshare',
                eventResource: 'Event',
                rideshare: ["$q", "$stateParams", "rideshareResource", function($q, $stateParams, rideshareResource) {
                    var deferred = $q.defer();
                    rideshareResource.authget({"id":$stateParams.id}).$promise.then(
                        function(rs) {
                            return deferred.resolve(rs);
                        },
                        function(error) {
                            console.error("Rideshare resolve failed in edit");
                            return deferred.reject(error);
                        }
                    );
                    return deferred.promise;
                }]
            }
        })
        .state('help', {
            parent: 'app',
            url:'help',
            views: {
                "@": {
                    controller: 'HelpController',
                    templateUrl: '/static/rideshare_frontend/views/help.html'
                }
            }
        })
        .state('faq', {
            parent: 'app',
            url:'faq',
            views: {
                "@": {
                    controller: 'FaqController',
                    templateUrl: '/static/rideshare_frontend/views/faq.html'
                }
            }
        });
}]);


// Check for cookie on user's first site access, set auth from cookie
app.run(['$rootScope', '$timeout', '$http', '$cookies', '$templateCache', '$interpolate', '$translate', '$state', '$preloaded', 'amMoment', 'tmhDynamicLocale',
    function($rootScope, $timeout, $http, $cookies, $templateCache, $interpolate, $translate, $state, $preloaded, amMoment, tmhDynamicLocale) {

    // xeditable options
    //editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
    //

    $rootScope.$on('modal', function(event, data) {
        //console.log('$on event', event )
        //console.log('$on data',  data)
        $rootScope.modal = data;
    });

    $rootScope.$on('$stateNotFound', function(ev, unfoundState, fromState, fromParams) {
        console.error("stateNotFound", ev, unfoundState, fromState, fromParams);
        $state.go('error');
    });

    $rootScope.$on('$stateChangeError', function(ev, toState, toParams, fromState, fromParams, error) {
//        console.log("ERROR in stateChange", error, ev, toState, fromState);
        if (angular.isObject(error)) {
            // send the toParams so that the event is known
            switch (error.status) {
                case 404:
                    // set the error object on the error state and go there
                    $state.get('event-error').error = error;
                    $state.go('event-error', toParams);
                    break;
                case 401:
                    // redirect to the detail page
                    $state.go('detail', toParams);
                    break;
                default:
                    // set the error object on the error state and go there
                    $state.get('error').error = error;
                    $state.go('error', toParams);
            }
        }
        else {
            // unexpected error
            $state.go('error', toParams);
        }

    });

    // Set default locale to whatever we got from $translateProvider -- either
    // set by .preferredLanguage or .determinePreferredLanguage .use() is a setter/getter
    // Moment is for *date* formatting by locale.
    // $translate.user() *gets* the current locale from LocalStorage (fallback: CookieStore).
    amMoment.changeLocale($translate.use());

    // Set the default locale to whatever was saved by translate.
    // tmhDynamicLocale is for numeric formatting (actually, it is for dynamic switching of locales, downloading them from
    // web site, as needed.  The locale identifier is already being stored by Moment, which seems to have a more flexible
    // storache scheme, so here we are using that one.
    tmhDynamicLocale.set($translate.use());


    // Listen for changeLanguage signal
    $rootScope.$on('changeLanguage', function(event, langKey) {
        //console.log('changing to', langKey)
        tmhDynamicLocale.set(langKey);
        amMoment.changeLocale(langKey);
        $translate.use(langKey);
        event.targetScope.main.preferredLanguage = langKey;
    });

    // Enable CORS
    $http.defaults.useXDomain = true;
    delete $http.defaults.headers.common['X-Requested-With'];

    // Sending CSRF Token if it is supplied -- may not be necessary, since using Token authentication
    // console.log('csrftoken', $cookies['csrftoken'], $cookies.csrftoken)
    $http.defaults.headers.post['X-CSRFToken'] = $cookies.csrftoken;

//    console.log('Sending event:initial-auth')
//    // Need to delay broadcast until directive has loaded
//    ($timeout(function() {
//        $rootScope.$broadcast('event:initial-auth');
//
//        // Enable CORS
//        $http.defaults.useXDomain = true;
//        delete $http.defaults.headers.common['X-Requested-With'];
//
//        // Sending CSRF Token if it is supplied -- may not be necessary, since using Token authentication
//        $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
//    },100))();
}]);

