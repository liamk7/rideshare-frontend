'use strict';

/* Providers */

var mod = angular.module('rideshares.providers', []);

mod.value('version', '0.1');

mod.provider('Environment', function () {
    var environments = {
        dev: {
            root: 'http://localhost',
            port: 8000,
            api: '/api',
            version: ''
        }
    };
    var selectedEnv = 'dev';
    var self = this;

    this.setEnvironments = function (envs) {
        if (!Object.keys(envs).length)
            throw new Error('At least one environment is required!');

        environments = envs;
    };

    this.setActive = function (env) {
        if (!environments[env])
            throw new Error('No such environment present: ' + env);

        selectedEnv = env;
        return self.getActive();
    };

    this.getEnvironment = function (env) {
        if (!env)
            throw new Error('No such environment present: ' + env);
        return environments[env];
    };

    this.getActive = function () {
        if (!selectedEnv)
            throw new Error('You must configure at least one environment');

        return environments[selectedEnv];
    };

    this.getApiRoute = function () {
        var active = self.getActive();
        return active.root + (active.port ? ':' + active.port : '') +
            active.api + (active.version ? '/' + active.version : '');
    };

    this.getDefaultRoute = function () {
        var active = self.getActive();
        return active.root + (active.port ? ':' + active.port : '');
    };

    this.$get = [function () {
        return self;
    }];
})
