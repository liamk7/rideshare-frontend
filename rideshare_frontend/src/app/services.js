'use strict';

/* Services */

var mod = angular.module('rideshares.services', []);

mod.value('version', '0.1');

mod.factory('Rideshare', ['$resource', 'Environment', function ($resource, Environment) {
        return $resource(Environment.getApiRoute() + '/rideshares/:id/', { id: "@id", slug:"@slug" },
            {
                authget: {
                    url: Environment.getApiRoute() + '/rideshares/:id/force_auth/',
                    method: 'GET'
                },
                update: {
                    method: 'PUT'
                }
            }
        );
    }
]);

mod.factory('Contact',
    ['$resource', 'Environment', function ($resource, Environment) {
        return $resource(Environment.getApiRoute() + '/contacts/');
    }
]);

mod.factory('Help',
    ['$resource', 'Environment', function ($resource, Environment) {
        return $resource(Environment.getApiRoute() + '/help/');
    }
]);

/*mod.factory('User',
    ['$resource', function ($resource) {
        return $resource('/api/users/:id', {id: '', username:'@username'}, null, {
            get: {

            }});
    }
    ]);*/

mod.factory('Preference',
    ['$resource', 'Environment', function ($resource, Environment) {
        return $resource(Environment.getApiRoute() + '/preferences/:id', {id: ''}, {
            query: {
                method:'GET',
                params:{'format':'json'},
                isArray:true,
                cache: true
            },
            get: {
                method:'GET',
                params:{'format':'json'},
                isArray:false,
                cache: true
            }
        });
    }
]);

mod.factory('Faq',
    ['$resource', 'Environment', function ($resource, Environment) {
        return $resource(Environment.getApiRoute() + '/faqs/:id', {id: ''}, {
            query: {
                method:'GET',
                params:{'format':'json'},
                isArray:true,
                cache: true
            },
            get: {
                method:'GET',
                params:{'format':'json'},
                isArray:false,
                cache: true
            }
        });
    }
]);

mod.factory('Event',
    ['$resource', 'Environment', function ($resource, Environment) {
        //console.log(Environment.getActive());
        //console.log(Environment.getApiRoute());
        return $resource(Environment.getApiRoute() + '/events/:slug/', {slug:'@slug'},  {
        //return $resource('http://localhost:8000/api' + '/events/:slug/', {slug:'@slug'},  {
            query: {
                method:'GET',
                params:{'format':'json'},
                isArray:true,
                cache: true
            },
            get: {
                method:'GET',
                params:{'format':'json'},
                isArray:false,
                cache: true
            }
        });
    }
]);

mod.factory('Stats',
    ['$resource', '$rootScope', 'Environment', function ($resource, $rootScope, Environment) {
        return $resource(Environment.getApiRoute() + '/aggregate/', {official:true});
    }
]);

mod.factory('Region',
    ['$resource', 'Environment',  function ($resource, Environment) {
        return $resource(Environment.getApiRoute() + '/regionals/:slug/', {slug:'@slug'},  {
            query: {
                method:'GET',
                isArray:true,
                params:{'format':'json'},
                cache: true
            },
            get: {
                method:'GET',
                params:{'format':'json'},
                isArray:false,
                cache: true
            }
        });
    }
]);

// Get a location (either airport or city) via typeahead
mod.factory('AutoLocation', ['$http', 'Environment', function($http, Environment) {
    return {
        get: function(val, airport, rsevent) {
            var url = airport ? Environment.getApiRoute() + '/autoairports/' :
                                Environment.getApiRoute() + '/autocities/';
            return $http.get(url, {
                params: {
                    name: val,
                    e_id: rsevent ? rsevent.id : null
                }
            });
        }
    };
}]);

// Get a Rideshare via typeahead
mod.factory('AutoRideshare', ['$http', 'Environment', function($http, Environment) {
    return {
        get: function(val, event_slug, flight) {
            // accept 'r' or 'f', or true/false for flight var
            if (typeof(flight) === "string") { flight = flight === 'f'; }
            return $http.get(Environment.getApiRoute() + '/autorideshares/', {
                params: {
                    name: val,
                    slug: event_slug,
                    flight: flight
                }
            });
        }
    };
}]);


mod.factory('MainScope', ['$translate', '$filter', '$preloaded',
    function ($translate, $filter, $preloaded) {
        // Serve up the options *without* the "Any" which is used in the searchForm
        var makeRealOptions = function(obj) {
            return obj.filter(function(x) {return x.id >= 0; });
        };

//        this.$storage = $localStorage.$default({});

        var constants = {
//            "preferredMassUnit": "kg",
//            "preferredDistanceUnit": "km",
//            "locationPlaceholder": "My Location",
//            "destinationPlaceholder": "My destination",
//            "searchPlaceholder": "Search by Name",
//            "numPassengersPlaceholder": "How many?",
            "tofromOptions": [
            {
                id: 0,
                name: "to",
                key: "TO",
                activity_key: "TO_EVENT",
                reverse_key: "FROM",
                locationPlaceholder: "MY_LOCATION"
            },
            {
                id: 1,
                name: "from",
                key: "FROM",
                activity_key: "FROM_EVENT",
                reverse_key: "TO",
                locationPlaceholder: "MY_DESTINATION"
            }
            ],
            "roundTripOptions": [
                { id: -1, key: "ANY", name: "Any" },
                { id: 0, key: "ONE_WAY", name: "One Way"},
                { id: 1, key: "ROUND_TRIP", name: "Round Trip"}
            ],
            "rideTypeOptions": [
                { id:-1, key: "ANY", name:"Any"},
                { id:0, key: "OFFER",name:"Offer"},
                { id:1, key: "REQUEST",name:"Request"}
            ],
            "haulingOptions": [
                { id:-1, key: "ANY", name:"Any"},
                { id:0, key: "NO", name:"No"},
                { id:1, key: "YES", name:"Yes"}
            ],
            "flightOptions": [
                { id:-1, key: "ANY", name:"Any"},
                { id:0, key: "NO", name:"No"},
                { id:1, key: "YES", name:"Yes"}
            ],
            "rideOptions": [
                { id:-1, key: "ANY", name:"Any"},
                { id:0, key: "NO", name:"No"},
                { id:1, key: "YES", name:"Yes"}
            ]
        };
        // The option values correspond to the object ids in the database.  The -1 values are here for use in the search
        // filter, where "Any" may be selected.
        var realOptions = {
            'rideTypeOptions':makeRealOptions(constants.rideTypeOptions),
            'haulingOptions':makeRealOptions(constants.haulingOptions),
            'flightOptions':makeRealOptions(constants.flightOptions),
            'tofromOptions':makeRealOptions(constants.tofromOptions),
            'roundTripOptions':makeRealOptions(constants.roundTripOptions),
            'rideOptions':makeRealOptions(constants.rideOptions)
        };
//
        return {
            // a couple of tweaks when running as kiosk
            'kiosk': $preloaded.kiosk,
            'gravatarSize': $preloaded.kiosk ? '80px' : '55px',

            "preferredMassUnit": "lbs",
//            "preferredMassUnit": $localStorage.$default({unit:"lbs"}),
            "preferredDistanceUnit": "mi",
            "preferredLanguage": $translate.use(), // init to whatever translate is set to
            "dateFormat": "fullDate",

            // These are set by resolve
            "region": null,
            "rf": null,
            "event": null,
            // This is set when a detail page is loaded
            "rideshare": null,
            // Add default, since page is displayed before messages are loaded
            "messages": [],

            "formParams": {
                "radius":20,
                "showAdvanced": true,
                "rt": null,
                "event":null,
                "location": null,
                "rideType": null,
                "hauling": null,
                "ride": null,
                "flight": null,
                "tofrom":constants.tofromOptions[0] // need this for activity table
            },

            "user": {
                isLoggedIn: false
            },
            "constants": constants,
            "realOptions": realOptions,
            "currentPage": 1,

            "activity": {},
            "forceFilter": {},
            "swapToFrom": function () {
                this.formParams.tofrom = $filter('filter')(this.constants.tofromOptions, {key: this.formParams.tofrom.reverse_key})[0];
            },
            "swapMassUnit": function () {
                this.preferredMassUnit = this.preferredMassUnit === 'lbs' ? 'kg' : 'lbs';
            }
        };
    }]
);

// Deprecated, not used
//mod.service('unitConversion', function() {
//    //console.log('Creating unitConversion')
//    this.mi2k = function(unit) {
//        return "mi2k" + (unit * 1.6);
//    };
//    this.lb2kg = function(unit) {
//        //console.log('lb2kg!!!', typeof unit)
//        return "lb2kg" + (unit / 2.2046);
//    };
//});
