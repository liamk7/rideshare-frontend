'use strict';

/* Directives */

//var mod = angular.module('rideshares.directives', ['http-auth-interceptor'])
var mod = angular.module('rideshares.directives', []);

mod.directive('appVersion', ['version', function(version) {
    return function(scope, elm, attrs) {
      elm.text(version);
    };
}]);

// Entering maximum baggage weight.  See also the metric filter.
mod.directive('metricInput', function() {
    return {
        restrict: 'A',
        require: 'ngModel',
        scope: {
            preferredUnit: '@'
        },
        link : function(scope, element, attr, ngModelCtrl) {
            scope.$watch('preferredUnit', function (newVal, oldVal) {
                if (newVal && newVal !== oldVal) {
                    ngModelCtrl.$setViewValue(Math.round(newVal === 'lbs' ? element.val() * 2.2046 : element.val() / 2.2046));
                    ngModelCtrl.$render();
                }
            });
            ngModelCtrl.$parsers.unshift(function (val) {
                return scope.preferredUnit === 'lbs' ? val / 2.2046 : val;

            });
            ngModelCtrl.$formatters.unshift(function (val) {
                return Math.round(scope.preferredUnit === 'lbs' ? val * 2.2046 : val);
            });
        }
    };
});

// Just for conveniently changing the event name text treatment
mod.directive('eventLink', function() {
    return {
        restrict: 'EA',
        scope: {
            nolink: '@',
            event: '='
        },
        template:
            '<h3 ng-if="!nolink"><a ui-sref="event({slug: event.slug})"><span ng-style="\'white-space:nowrap\'">{{event.name | uppercase}}</span><br class="visible-xs"/> <span ng-style="\'white-space:nowrap\'" class="text-muted">{{event.location.name}}, {{event.location.city.country.continent.code == "NA" ? event.location.city.region.code : event.location.city.country.name }}</span></a></h3>' +
            '<h3 ng-if="nolink"><span ng-style="\'white-space:nowrap\'">{{event.name | uppercase}}</span><br class="visible-xs"/> <span ng-style="\'white-space:nowrap\'" class="text-muted">{{event.location.name}}, {{event.location.city.country.continent.code == "NA" ? event.location.city.region.code : event.location.city.country.name}}</span></h3>'
            //'<h3 ng-if="!nolink"><a ui-sref="event({slug: event.slug})"><span ng-style="\'white-space:nowrap\'">{{event.name | uppercase}}</span> <span ng-style="\'white-space:nowrap\'" class="text-muted">{{event.location.name}}, {{event.location.city.country.continent.code == "NA" ? event.location.city.region.code : event.location.city.country.name }}</span></a></h3>' +
            //'<h3 ng-if="nolink"><span ng-style="\'white-space:nowrap\'">{{event.name | uppercase}}</span> <span ng-style="\'white-space:nowrap\'" class="text-muted">{{event.location.name}}, {{event.location.city.region.code}}</span></h3>'
    };
});

// Make sure the two password fields match.
mod.directive("compareTo", function() {
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=compareTo"
        },
        link: function(scope, element, attributes, ngModel) {

            ngModel.$validators.compareTo = function(modelValue) {
                return modelValue === scope.otherModelValue;
            };

            scope.$watch("otherModelValue", function() {
                ngModel.$validate();
            });
        }
    };
});

mod.directive('backButton', function(){
    return {
        restrict: 'A',

        link: function(scope, element, attrs) {
            element.bind('click', goBack);

            function goBack() {
                history.back();
                scope.$apply();
            }
        }
    };
});

mod.directive('navbar', ['$preloaded', function($preloaded){
    return {
        restrict: "E",
        replace: true,
        transclude: true,
        templateUrl: "/static/rideshare_frontend/partials/navbar.html",
        link: function(scope, element, attrs) {
            var li, liElements, links, index, length;
            var searchForm = element.find("#findRideshareForm");

            // set class of the current navigation tab to "active"
            liElements = element.find("#navigation-tabs li");
            for (index = 0, length = liElements.length; index < length; index++) {
                li = liElements[index];
                links = $(li).find("a");
                // toUpper() is required as a result of using translate identifiers, e.g. HOME which must match Home.
                if (links.length > 0 && links[0].textContent.toUpperCase() === attrs.currentTab.toUpperCase()) $(li).addClass("active");
            }
            // Show the search input element if we're on the search page
            if (attrs.currentTab === 'Search') {
                searchForm.show();
            }
            else {
                searchForm.hide();
            }
            if ($preloaded.kiosk) {
                var homeButton = element.find("#homeButton");
                var helpButton = element.find("#helpButton");
                homeButton.hide();
                helpButton.hide();
            }
        }
    };
}]);
