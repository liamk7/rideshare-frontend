'use strict';

/* Filters */

var mod = angular.module('rideshares.filters', []);

mod.filter('to_trusted', ['$sce', function($sce){
    return function(text) {
        return $sce.trustAsHtml(text);
    };
}]);

mod.filter('getByProperty', function() {
    return function(propertyName, propertyValue, collection) {
        for (var i=0; i<collection.length; i++) {
            if (collection[i][propertyName] === propertyValue) {
                return collection[i];
            }
        }
        return null;
    };
});

mod.filter('getByRegex', function() {
    return function(propertyName, propertyValue, collection) {
        var filtered = [];
        for (var i=0; i<collection.length; i++) {
            var item = collection[i];
            var reg = new RegExp(propertyValue);
            if (reg.test(item[propertyName])) {
                filtered.push(item);
            }
        }
        return filtered;
    };
});

mod.filter('interpolate', ['version', function(version) {
    return function(text) {
      return String(text).replace(/%VERSION%/mg, version);
    };
  }]);


/**
 * Truncate Filter
 * @Param text
 * @Param length, default is 10
 * @Param end, default is "..."
 * @return string
 */
mod.filter('truncate', function () {
    return function (text, length, end) {
        if (isNaN(length))
            length = 10;

        if (end === undefined)
            end = "...";

        if (text.length <= length || text.length - end.length <= length) {
            return text;
        }
        else {
            return String(text).substring(0, length-end.length) + end;
        }

    };
});

/**
 * Unit Filter
 * @Param input
 * @Param unit (km or mi, kg or lbs)
 * @Param precision
 * @return string
 */
mod.filter('unit', function () {
    return function (input, unit, precision) {
//        console.log('Calling unit filter', input, unit, precision);
        if (!unit) { unit = "?"; }
        if (precision === undefined) { precision = 1; }
        input = parseFloat(input);
        if (isNaN(input)) {
            return "Nan";
        }
        else {
            var ret = input;
            if (unit === 'lbs') // convert to kg
                ret = (input / 2.2046);
            else if (unit === 'kg')
                ret = (input * 2.2046);
            else if (unit === 'mi' || unit === 'miles') // convert to kilometers
                ret = (input * 0.621371);
            else if (unit === 'km')
                ret = (input / 0.621371);
            else
                return "did not understand unit";

            if (precision === undefined) {
                return Math.round(ret);
            }
            else {
                return ret.toFixed(precision);
            }
        }
    };
});

// Convert values to metric if that is the unit requested, otherwise
// just return the value.
// In both cases, the value is rounded or fixed precision, as specified.
mod.filter('metric', function () {
    return function (input, unit, precision) {
        //console.log('unitDisplay', input, unit, precision);
        // convert TO the unit
        if (!unit) { unit = "?"; }
        input = parseFloat(input);
        if (isNaN(input)) {
            return "Nan";
        }
        else {
            var ret = input;
            if (unit === 'lbs') { // convert to kg
                ret = (input * 2.2046);
            }
            else if (unit === 'mi' || unit === 'miles') { // convert to kilometers
                ret = (input / 0.621371);
            }

            if (precision === undefined) {
                return Math.round(ret);
            }
            else {
                return ret.toFixed(precision);
            }
        }
    };
});

/**
 * Dist Filter
 * @Param input
 * @Param lat1
 * @Param lon1
 * @Param lat2
 * @Param lon2
 * @return string
 */
//mod.filter('dist', function () {
//    function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
//        var R = 6371; // Radius of the earth in km
//        var dLat = deg2rad(lat2-lat1);  // deg2rad below
//        var dLon = deg2rad(lon2-lon1);
//        var a =
//                Math.sin(dLat/2) * Math.sin(dLat/2) +
//                    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
//                        Math.sin(dLon/2) * Math.sin(dLon/2)
//            ;
//        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
//        var d = R * c; // Distance in km
//        return d;
//    }
//
//    function deg2rad(deg) {
//        return deg * (Math.PI/180)
//    }
//    return function (input, lat1,lon1,lat2,lon2) {
//        if (!(lat1 && lon1 && lat2 && lon2)) { return "Bad args."}
//        return getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2)
//    };
//});
