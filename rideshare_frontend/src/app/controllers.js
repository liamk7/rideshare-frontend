'use strict';

/* Controllers */

var mod = angular.module('rideshares.controllers', ['ui.router', 'ngResource', 'ngAnimate']);


// Borrowed from:
// https://stackoverflow.com/questions/20498317/user-authentication-in-django-rest-framework-angular-js-web-app

mod.controller('LoginController', [
     '$scope', '$http', '$cookieStore', '$window', '$uibModalInstance', 'MainScope', 'Contact', 'Environment', function(
       $scope, $http, $cookieStore, $window, $uibModalInstance, MainScope, Contact, Environment) {

        $scope.main = MainScope;
        $scope.login = { 'username':'', 'password':'', 'error': '', 'obj_cls': 'Rideshare', 'obj_id': $scope.main.rideshare.id };

        var clearLoginInfo = function (errMsg) {
            //console.log('Clearing Login Info')
            if (typeof errMsg === 'undefined') {
                errMsg = '';
            }
            $scope.login.username = '';
            $scope.login.password = '';
            $scope.login.error = errMsg;
        };

        $scope.close =  function(result) {
            if (typeof(result) === 'undefined') { clearLoginInfo(); $uibModalInstance.dismiss('cancel');  }
            else {
                // add these to scope for the login form
                // password value is shared for both types of authentication
                // i.e. it's either (password + username) or (password + obj_cls + obj_id)
                var userData = result;

                $http({
                    url: Environment.getApiRoute() + "/login/",
                    method:"POST",
                    data: userData,
                    contentType:"application/json",
                    headers: {"Content-Type":'application/json'},
                    processData:false,
                    ignoreAuthModule: true,
                    config:{"Authorization": ""}})
                    .then(function(response) {
                        var token = response.headers().token;
                        $cookieStore.put('djangotoken', token);
                        $http.defaults.headers.common.Authorization = 'Token ' + token;
                        if (token) {
                            clearLoginInfo();
                            delete userData.password;
                            // need to save the obj_id because the user is only authorized to edit/view THIS obj.
                            // The obj_id then gets checked in the detail and create page.
                            $scope.main.user = userData;
    //                        $window.sessionStorage["userToken"] = JSON.stringify(token);
                            $uibModalInstance.close();

                            // Auth was successful, so check for messages
                            Contact.query({"rsid":$scope.main.rideshare.id}).$promise.then(function(result) {
                                $scope.main.messages = result || [];
                            }, function(result) {
                                console.error('Contact.query failed', result);
                            });
                        }
                        else {
                            console.warn('HTTP status == 200, but no Token!  Should not happen.');
                            console.warn('http response', response);
                        }

                    }, function(data, status) {
                        if (!data.isLogged) {
                            var errMsg = 'INVALID_LOGIN';
                            clearLoginInfo(errMsg);
                            console.error('Invalid login', errMsg, status);
                            // just to be sure...
                            if ($scope.main.user) {
                                $scope.main.user.isLoggedIn = false;
                            }
                        }
                    }
                );
            }
        };

//        $scope.close =  function(result) {
//            //console.log('Canceling Login')
//            clearLoginInfo()
//            $rootScope.$emit('modal')
//        };
//
//        $scope.submit = function() {
//            // add these to scope for the login form
//            // password value is shared for both types of authentication
//            // i.e. it's either (password + username) or (password + obj_cls + obj_id)
//            var userData = {
//                "username": $scope.login.username,
//                "password": $scope.login.password,
//                "obj_cls": "Rideshare",
//                "obj_id": $scope.main.rideshare.id
//            };
//            //console.log('USER DATA', userData)
//
//            $http({
//                url:"/api/login/",
//                method:"POST",
//                data: userData,
//                contentType:"application/json",
//                headers: {"Content-Type":'application/json'},
//                processData:false,
//                ignoreAuthModule: true,
//                config:{"Authorization": ""}})
//                .then(function(response) {
//                    var token = response.headers().token
//                    $cookieStore.put('djangotoken', token);
//                    $http.defaults.headers.common['Authorization'] = 'Token ' + token;
//                    if (token) {
//                        clearLoginInfo();
//                        delete userData.password
//                        // need to save the obj_id because the user is only authorized to edit/view THIS obj.
//                        // The obj_id then gets checked in the detail and create page.
//                        $scope.main.user = userData;
////                        $window.sessionStorage["userToken"] = JSON.stringify(token);
//                        $rootScope.$emit('modal')
//
//                        // Auth was successful, so check for messages
//                        Contact.query({"rsid":$scope.main.rideshare.id}).$promise.then(function(result) {
//                            //console.log('Success')
//                            $scope.main.messages = result
//                        }, function(result) {
//                            //console.log('Error')
//                        })
//                    }
//                    else {
//                        console.warn('HTTP status == 200, but no Token!  Should not happen.')
//                    }
//
//                }, function(data, status) {
//                    //console.log('data.isLogged', data.data.isLogged)
//                    if (!data.isLogged) {
//                        var errMsg = 'INVALID_LOGIN'
//                        clearLoginInfo(errMsg)
//                        // just to be sure...
//                        if ($scope.main.user) {
//                            $scope.main.user.isLoggedIn = false;
//                        }
//                    }
//                    //console.log('DATA', data);
//                    //console.log('HEADERS', data.headers)
//                    //console.log('STATUS', status)
//                }
//            );
//        };
}]);

mod.controller('ContactController', ['$scope', '$uibModalInstance', 'MainScope', function
    ($scope, $uibModalInstance, MainScope) {
    $scope.main = MainScope;
    $scope.main.contactFormParams = { "rideshare": $scope.main.rideshare.id };

    $scope.contactFormUnchanged = function () {
    };

    $scope.close =  function(result) {
        if (typeof(result) === 'undefined') { $uibModalInstance.dismiss('cancel');  }
        else { $uibModalInstance.close($scope.main.contactFormParams);  }
    };
}]);

mod.controller('DeleteController', ['$scope', '$uibModalInstance', 'MainScope', function
    ($scope, $uibModalInstance, MainScope) {
    $scope.main = MainScope;

    $scope.close =  function(result) {
        if (typeof(result) === 'undefined') { $uibModalInstance.dismiss('cancel');  }
        else { $uibModalInstance.close();  }
    };
}]);

mod.controller('FilledController', ['$rootScope', '$scope', '$stateParams', '$http', 'MainScope', function
    ($rootScope, $scope, $stateParams, $http, MainScope) {
    //console.log('FilledController')
    $scope.main = MainScope;

    $scope.close =  function(result) {
        if (typeof(result) === 'undefined') { $rootScope.$emit('modal'); return; }
        var url = "/api/rideshares/" + $stateParams.id + "/";
        var newVal = !$scope.main.rideshare.full;
        $http.patch(url, {"full": newVal})
            .success(function (data, status) {
                $scope.main.rideshare.full = newVal;
            })
            .error(function (data, status) {
                console.error('Filled error', data, status)
            }
        );
        $rootScope.$emit('modal');
    };
}]);

// This just reports what happened with the Help or Contact submission
mod.controller('FeedbackController', ['$rootScope', '$scope', 'MainScope', function($rootScope, $scope, MainScope) {
    $scope.main = MainScope;
    $scope.close =  function(result) {
        $rootScope.$emit('modal');
    };
}]);

// This just reports what happened with the Help or Contact submission
mod.controller('ErrorController', ['$state', '$scope', 'MainScope', function($state, $scope, MainScope) {
    $scope.main = MainScope;
    if (angular.isObject($state.current.error)) {
        $scope.error = $state.current.error;
        // could just use the error.statusText and replace ' ' with '_'
        switch ($state.current.error.status) {
            case 404:
                $scope.errorMsgKey = 'NOT_FOUND';
                break;
            case 401:
                $scope.errorMsgKey = 'UNAUTHORIZED';
                break;
            default: {
              break;
            }
        }
    }
}]);


mod.controller('DetailController', [
        '$rootScope', '$scope', '$http', '$uibModal', '$timeout', '$state', 'MainScope', 'Rideshare', 'Contact', 'event', 'rideshare', function(
         $rootScope, $scope, $http, $uibModal, $timeout, $state, MainScope, Rideshare, Contact, event, rideshare) {

    $scope.pageName = "Detail Page";
    $scope.pageClass = "page-detail";

    $scope.main = MainScope;
    $scope.main.rideshare = $scope.rs = rideshare;
    $scope.main.event = event;

    $scope.main.contactFormParams = { "rideshare": rideshare };

    $scope.filled = function () {
        $rootScope.$emit('modal', '/static/rideshare_frontend/partials/filled_modal.html');
    };
    $scope.delete = function () {
    };

    // Open the Delete modal
    $scope.delete = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/static/rideshare_frontend/partials/delete_modal.html',
            controller: 'DeleteController',
            scope: $scope
        });

        modalInstance.result.then(function () {
            $scope.main.apiResponse = null;
            $rootScope.$emit('modal', '/static/rideshare_frontend/partials/feedback_modal.html');
            Rideshare.delete({"id":rideshare.id}).$promise.then(function(result) {
                $scope.main.apiResponse = {
                    "success": true,
                    "headerKey": "SUCCESS",
                    "messageKey": "LISTING_SUCCESS_MSG",
                    "operation": "delete"
                };
                $timeout(function(){
                    $rootScope.$emit('modal');
                    $scope.main.apiResponse = null;
                }, 5000);
                // after deleting, go back to current event page
                $state.go('event', {slug: $scope.main.event.slug});

            }, function(result) {
                $scope.main.apiResponse = {
                    "success": false,
                    "headerKey": "FAILURE",
                    "messageKey": "GENERAL_FAILURE_MSG"
                };
                $timeout(function(){
                    $rootScope.$emit('modal');
                    $scope.main.apiResponse = null;

                }, 5000);
            });


        }, function () {
        });
    };

    $scope.close = function () {
        //console.log('close() emitting modal')
        $rootScope.$emit('modal');
    };

    // Open the Contact modal
    $scope.openContactModal = function () {
        $scope.contactFormParams = {'rsid': rideshare.id};
        var modalInstance = $uibModal.open({
            templateUrl: '/static/rideshare_frontend/partials/contact_modal.html',
            controller: 'ContactController',
            scope: $scope,
//            size: 'sm',
            resolve: {
                contactFormParams: function () {
                    return $scope.contactFormParams;
                }
            }
        });

        modalInstance.result.then(function (contactFormParams) {
            // Note: contact_phone should include country dialCode, e.g. "1" for US.
            // Currently, this works via directive using nationalMode attr.
            var contact = {
                "rideshare": contactFormParams.rideshare,
                "contact_name": contactFormParams.contact_name,
                "contact_email": contactFormParams.contact_email,
                "contact_info": contactFormParams.contact_info,
                "contact_phone":
                    // only add the '+' if there is a phone number
                    contactFormParams.contact_phone ? contactFormParams.contact_phone.replace(/^[+]?/, '+') : null,
                "message": contactFormParams.message
            };
            $scope.main.apiResponse = null;
            $rootScope.$emit('modal', '/static/rideshare_frontend/partials/feedback_modal.html');
            Contact.save(contact).$promise.then(function (result) {
                $scope.main.apiResponse = {
                    "success": true,
                    "headerKey": "SUCCESS",
                    "messageKey": "SEND_SUCCESS_MSG"
                };
                $timeout(function(){
                    $rootScope.$emit('modal');
                    $scope.main.apiResponse = null;

                }, 5000);
            }, function (result) {
                $scope.main.apiResponse = {
                    "success": false,
                    "headerKey": "FAILURE",
                    "messageKey": "SEND_FAILURE_MSG"
                };
                $timeout(function(){
                    $rootScope.$emit('modal');
                    $scope.main.apiResponse = null;

                }, 5000);
            });


        }, function () {
        });
    };

    // Open the Login modal
    $scope.openLoginModal = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/static/rideshare_frontend/partials/login_modal.html',
            controller: 'LoginController',
            scope: $scope,
//            size: 'sm',
            resolve: {
                contactFormParams: function () {
                    return $scope.login;
                }
            }
        });
        modalInstance.result.then(function (result) {
        });
    };

    $scope.edit = function () {
        if (rideshare.id) {
            // auth re-get the rideshare in the resolve for edit state, so it has the private data
            $state.go('edit', {id: rideshare.id});
        }
    };

    $scope.showMessages = function() {
       // Only set messages if query was successful (i.e. it was authenticated)
       Contact.query({"rsid":$scope.main.rideshare.id}).$promise.then(function(result) {
           $scope.main.messages = result || [];
       }, function(result) {
          console.error('showMessages', result)
       });
    };
    $scope.logout = function() {
        $http.defaults.headers.common.Authorization = null;
        $scope.main.user = null;
        $scope.main.contactFormParams = null;
//        $window.sessionStorage["userToken"] = null;
    };

}]);


mod.controller('HomeController',['$scope', '$filter', 'MainScope', 'region',
    function($scope, $filter, MainScope, region) {
        $scope.main = MainScope;
        $scope.main.region = region;

        // reset the main event, forcing user to pick event from pulldown or image link
        $scope.main.event = null;

        // current events are ones that are displayed on the home page
        var currentEvents = $filter('filter')($scope.main.region.event_set, {current: true});

        // featured event should be set, however, if it's not set use the first currentEvent
        var featured = $filter('filter')($scope.main.region.event_set, {featured: true});

        if (featured && featured.length === 1) {
            $scope.featuredEvent = featured[0];
        }
        else if (currentEvents && currentEvents.length >= 1) {
            $scope.featuredEvent = currentEvents.shift();
        }
        else {
            $scope.featuredEvent = null;
        }

        // limit number of events displayed, if you like
        // currently allowing this to be set in database
        // $scope.current_events = current.slice(0,3)
        $scope.currentEvents = currentEvents;

        $scope.pageClass = "page-home";

    }]);

mod.controller('SiteSearchController',['$scope', 'MainScope', 'region', 'opensearch_rs',
    function($scope,  MainScope, region, opensearch_rs) {
        $scope.main = MainScope;
        $scope.main.region = region;
        $scope.opensearch_rs = opensearch_rs;

        // reset the main event, forcing user to pick event from pulldown or image link
        $scope.main.event = null;

        $scope.pageClass = "page-home";

    }]);

mod.controller('FaqController',['$scope', '$sce', 'Faq', 'MainScope',
    function($scope, $sce, Faq, MainScope) {
        $scope.pageClass = "page-faq";
        $scope.main = MainScope;

        // This watcher fires on page load, and whenever the language is changed
        $scope.$watch("main.preferredLanguage", function() {
            Faq.query({"language":$scope.main.preferredLanguage}).$promise.then(
                function(faqs) {
                    var my_faqs = [];
                    for(var i=0; i<faqs.length; i++) {
                        var q = $sce.trustAsHtml(faqs[i].question);
                        var a = $sce.trustAsHtml(faqs[i].answer);
                        my_faqs.push({question:q, answer:a});
                    }
                    $scope.main.faqs = my_faqs;
                }
            );
        });
    }]);

mod.controller('HelpController',['$rootScope', '$scope', '$timeout', 'MainScope', 'Help',
    function($rootScope, $scope, $timeout, MainScope, Help) {
        $scope.pageClass = "page-help";
        $scope.main = MainScope;
        $scope.main.commentForm = {};

        $scope.submit = function(commentForm){
            $scope.main.apiResponse = null;
            $rootScope.$emit('modal', '/static/rideshare_frontend/partials/feedback_modal.html');

            Help.save(commentForm).$promise.then(function (result) {
                $scope.main.apiResponse = {
                    "success": true,
                    "headerKey": "SUCCESS",
                    "messageKey": "SEND_SUCCESS_MSG"
                };
                $timeout(function(){
                    $rootScope.$emit('modal');
                    $scope.main.apiResponse = null;
                    // blank out the form on success, but not on failure (give them a chance to correct/resubmit)
                    $scope.main.commentForm = {};
                    $scope.commentForm.$setPristine();
                    $scope.commentForm.$setUntouched();
                }, 5000);
            }, function (result) {
                console.error('Mail sending failure', result);
                $scope.main.apiResponse = {
                    "success": false,
                    "headerKey": "FAILURE",
                    "messageKey": "SEND_FAILURE_MSG"
                };
                $timeout(function(){
                    $rootScope.$emit('modal');
                    $scope.main.apiResponse = null;
                }, 5000);
            });
        };
    }]);

mod.controller('RegionHeaderController',['$scope', 'MainScope',  'region',
    function($scope, MainScope, region) {
        $scope.pageClass = "page-event";
        $scope.main = MainScope;
        $scope.main.region = region;
    }]);

mod.controller('RegionHomeController',['$scope', 'MainScope',  'region',
    function($scope, MainScope, region) {
        $scope.pageClass = "page-event";
        $scope.main = MainScope;
        $scope.main.region = region;
    }]);

mod.controller('EventHeaderController',['$scope', 'MainScope',  'event',
    function($scope, MainScope, event) {
        $scope.pageClass = "page-event";
        $scope.main = MainScope;
        $scope.main.event = event;
    }]);

mod.controller('EventHomeController',['$scope', '$filter', '$state', 'MainScope', 'Stats', 'region', 'event',
    function($scope, $filter, $state, MainScope, Stats, region, event) {
        $scope.pageClass = "page-event";

        $scope.main = MainScope;
        $scope.main.region = region;
        $scope.main.event = event;

        // Calculate the duration of the event in hours, and add to event as date_difference
        // (Don't display end date if date_difference is less than 24 hours -- that logic is in template)
        $scope.main.event.date_difference = (Date.parse(event.end_date) - Date.parse(event.start_date)) / 1000 / 60 / 60;

        $scope.update = function (value) {
            $state.go($state.current, {param: value});
        };

        var activityFilter = function (rtype, flight) {
            $scope.main.forceFilter.rideType = $scope.main.constants.rideTypeOptions[rtype];
            $scope.main.forceFilter.flight = $scope.main.constants.flightOptions[flight];
            $state.go('search');
        };

        // Functions that set the appropriate filters and redirect to search page
        $scope.main.activity.searchOffers =         function () { activityFilter(1, 0); };
        $scope.main.activity.searchRequests =       function () { activityFilter(2, 0); };
        $scope.main.activity.searchCars =           function () { activityFilter(0, 1); };
        $scope.main.activity.searchPlanes =         function () { activityFilter(0, 2); };
        $scope.main.activity.searchCarOffers =      function () { activityFilter(1, 1); };
        $scope.main.activity.searchCarRequests =    function () { activityFilter(2, 1); };
        $scope.main.activity.searchPlaneOffers =    function () { activityFilter(1, 2); };
        $scope.main.activity.searchPlaneRequests =  function () { activityFilter(2, 2); };




        $scope.showEvent = function() {
            var selected = $filter('filter')($scope.main.region.event_set, {id: $scope.main.event.id});
            return (selected.length) ? selected[0].name : 'Not set';
        };

        $scope.$watch("main.event", function() {
            if (!$scope.main.event) { return; }
            Stats.query({event:$scope.main.event.slug}).$promise.then(
                function(stats) {
                    $scope.main.flightStats = $filter('filter')(stats, {airport: true})[0];
                    $scope.main.rideStats = $filter('filter')(stats, {airport: false})[0];
                }
            );
        });
}]);

mod.controller('SearchController',['$scope', '$http', '$filter', '$stateParams', '$location', '$anchorScroll', '$swipe', '$timeout', '$state', 'AutoLocation', 'AutoRideshare', 'MainScope', 'region', 'event', 'Environment',
        function($scope, $http, $filter, $stateParams, $location, $anchorScroll, $swipe, $timeout, $state, AutoLocation, AutoRideshare, MainScope, region, event, Environment) {
    $scope.pageName = "Search";
    $scope.pageClass = "page-search";

    $scope.$stateParams = $stateParams;

    $scope.search = {};
    $scope.main = MainScope;
    $scope.main.region = region;
    if (!$scope.main.event) {
        $scope.main.event = event;
//        $scope.pageChanged(null);
    }

    // paging
    $scope.main.totalServerItems = 0;
    $scope.main.noResults = true;

    $scope.main.formParams.filterDirty = false;
    $scope.main.formParams.disableNumPassengers = false;


    // This gets changed when a filter value is changed, via watch() functions, to trigger datasource.revision()
    var filterChange = 0;

    // Special init for <select> element because angular compares option values by reference to the object itself,
    // so we need to set them to the constant option objects
    if ($scope.main.formParams.tofrom === null) {
        $scope.main.formParams.tofrom = $scope.main.constants.tofromOptions[0];
    }
    if ($scope.main.formParams.rt === null) {
        $scope.main.formParams.rt = $scope.main.constants.roundTripOptions[0];
    }
    if ($scope.main.formParams.rideType === null) {
        $scope.main.formParams.rideType = $scope.main.constants.rideTypeOptions[0];
    }
    if ($scope.main.formParams.hauling === null) {
        $scope.main.formParams.hauling = $scope.main.constants.haulingOptions[0];
    }
    if ($scope.main.formParams.flight === null) {
        $scope.main.formParams.flight = $scope.main.constants.flightOptions[0];
    }
    if ($scope.main.formParams.ride === null) {
        $scope.main.formParams.ride = $scope.main.constants.rideOptions[0];
    }

    // forceFilter is called from the Activity Table.  Here we override the defaults that are set above.
    if ($scope.main.forceFilter) {
        if ($scope.main.forceFilter.rideType) {
            $scope.main.formParams.rideType = $scope.main.forceFilter.rideType;
        }
        if ($scope.main.forceFilter.flight) {
            $scope.main.formParams.flight = $scope.main.forceFilter.flight;
        }
    }

    // reset the searchForm filters to default values
    $scope.resetFilter = function () {
        $scope.main.formParams.rt = $scope.main.constants.roundTripOptions[0];
        $scope.main.formParams.rideType = $scope.main.constants.rideTypeOptions[0];
        $scope.main.formParams.hauling = $scope.main.constants.haulingOptions[0];
        $scope.main.formParams.flight = $scope.main.constants.flightOptions[0];
        $scope.main.formParams.ride = $scope.main.constants.rideOptions[0];
        $scope.main.formParams.numPassengers = null;
        $scope.main.formParams.filterDirty = false;
        $scope.main.forceFilter = {};
    };

    $scope.distanceFormat = function (value) {
        return $filter('unit')(value, $scope.main.preferredDistanceUnit, 0);
    };

    $scope.pageChanged = function (page) {
        if (typeof(page) === 'number') {
            $scope.main.currentPage = page;
        }

        var p = {
            pageNumber: $scope.main.currentPage,
            pageSize: 10
        };

        // numPassengers can == 0, so check for null
        if ($scope.main.formParams.numPassengers !== null) { p.riders = $scope.main.formParams.numPassengers; }
        // currently, listingName is not used, in favor of the compeletely separate seaarch-by-name using ElasticSearch
        if ($scope.main.formParams.listingName) { p.name = $scope.main.formParams.listingName; }

        if ($scope.main.formParams.location) {
            // location.id is actually city.id copied to location object.
            p.city = $scope.main.formParams.location.id;
            p.radius = $scope.main.formParams.radius;
        }

        if ($scope.main.formParams.tofrom.id !== -1) {
            p.direction = $scope.main.formParams.tofrom.id;
        }
        if ($scope.main.formParams.rt.id !== -1) {
            p.round_trip = $scope.main.formParams.rt.id;
        }
        if ($scope.main.formParams.rideType.id !== -1) {
            p.rtype = $scope.main.formParams.rideType.id;
        }
        if ($scope.main.formParams.ride.id !== -1) {
            p.ride = $scope.main.formParams.ride.id;
        }
        if ($scope.main.formParams.hauling.id !== -1) {
            p.hauling = $scope.main.formParams.hauling.id;
        }
        if ($scope.main.formParams.flight.id !== -1) {
            p.flight = $scope.main.formParams.flight.key === 'YES';
        }

        if ($scope.main.event) { p.slug = $scope.main.event.slug; }

        $scope.loading = true;
        $http({
            url: Environment.getApiRoute() + "/rideshares/",
            cache: false,
            method: "GET",
            params: p
        }).success(function(data) {
                $scope.main.totalItems = data.count;
                if (data.results.length > 0) {
                    $scope.main.items = data.results;
                }
                else {
                    $scope.main.items = [];
                    $scope.loading = false;
                }
                $scope.loading = false;
                // scroll to top of search-page div
                // Note: resetting the location.hash to it's old value is a hack to prevent the scroll change from
                // showing up in the browser's history.  That way, a single back click takes you back to the previous
                // page, instead of requiring two.
                if (page && data.results.length > 0) {
                    var old = $location.hash();
                    $location.hash('search-page');
                    $anchorScroll();
                    $location.hash(old);
                }

            }).error(function(data, status, headers, config) {
                console.error('Error return from /api/rideshares', data, status, headers, config);
                //alert(JSON.stringify(data));
                $scope.loading = false;
                $scope.main.totalItems = 0;
            });
    };

    // Don't reset to page 1 here, since this may be reloading after detail page.
    $scope.pageChanged(null);


    // This is what we were using for infinite scrolling.  Leave it here in case we want to go back.
/*
    $scope.datasource = {
        get: function(index, count, success) {
            //console.log('================> Calling datasource() index = ', index);

            setTimeout(function () {
                var sb = [];
                var p = {
                    pageIndex: index,
                    pageSize: count
                };

                // numPassengers can == 0, so check for null
                if ($scope.main.formParams.numPassengers != null) { p.riders = $scope.main.formParams.numPassengers }
                if ($scope.main.formParams.listingName) { p.name = $scope.main.formParams.listingName }

                if ($scope.main.formParams.location) {
                    p.city = $scope.main.formParams.location.pk
                    p.radius = $scope.main.formParams.radius
                }

                if ($scope.main.formParams.tofrom.id != -1) {
                    p.direction = $scope.main.formParams.tofrom.id
                }
                if ($scope.main.formParams.rt.id != -1) {
                    p.round_trip = $scope.main.formParams.rt.id;
                }
                if ($scope.main.formParams.rideType.id != -1) {
                    p.rtype = $scope.main.formParams.rideType.id;
                }
                if ($scope.main.formParams.ride.id != -1) {
                    p.ride = $scope.main.formParams.ride.id;
                }
                if ($scope.main.formParams.hauling.id != -1) {
                    p.hauling = $scope.main.formParams.hauling.id;
                }
                if ($scope.main.formParams.flight.id != -1) {
                    if ($scope.main.formParams.flight.key == 'YES') {
                        p.flight = true;
                    }
                    else {
                        p.flight = false;
                    }
                }

                if ($scope.main.event) { p.slug = $scope.main.event.slug }

                $http({
                    url: "/api/rideshares/",
                    method: "GET",
                    params: p
                }).success(function(data, status, headers, config) {
                        $scope.main.totalServerItems = data.count;
                        $scope.main.noResults = index == 0 && data.count == 0
                        if (data.results.length > 0) {
                            success(data.results);
                        }
                        else {
                            success([]);
                        }
                    }).error(function(data, status, headers, config) {
                        console.warn('Error return from /api/rideshares')
                        //alert(JSON.stringify(data));
                    });
            }, 100);
        },
        revision: function () {
            //console.log("Calling datasource.revision, filterChange =", filterChange)
            return filterChange;
        },
        test: function() {

        }
    }
*/

    // This always gets the city locations, ie. the airport arg to AutoLocation.get() is false.
    $scope.getLocation = function (val) {
        return AutoLocation.get(val, false, $scope.main.event).then(
            function (res) {
                var locations = [];
		console.log(res);
                angular.forEach(res.data.results, function (item) {
                    locations.push(item);
                });
                return locations;
            }
        );
    };

    // Get rideshares by looking them up in ElasticSearch with typeahead.
    $scope.getRideshare = function (val) {
        return AutoRideshare.get(val, $scope.main.event.slug, null).then(
            function (res) {
                var rideshares = [];
                angular.forEach(res.data.results, function (item) {
                    rideshares.push(item);
                });
                return rideshares;
            }
        );
    };




    // ====================  WATCHERS =======================

    // watch the search.rideshare value which is set by the search-by-name pulldown
    $scope.$watch("search.rideshare", function() {
        var rs = $scope.search.rideshare;
        if (rs && typeof(rs) !== "undefined" && typeof(rs) !== "string" ) {
            $state.go('detail', {id: rs.pk});
            $scope.search.rideshare = null;
        }
    });

    $scope.$watchGroup([
        "main.formParams.rt",
        "main.formParams.rideType",
        "main.formParams.hauling",
        "main.formParams.flight",
        "main.formParams.ride"],
        function(newVals) {
            if (typeof(newVals) === 'undefined') { return; }
            $scope.main.formParams.filterDirty = !($scope.main.formParams.rt.key === $scope.main.constants.roundTripOptions[0].key &&
                $scope.main.formParams.rideType.key === $scope.main.constants.rideTypeOptions[0].key &&
                $scope.main.formParams.hauling.key === $scope.main.constants.haulingOptions[0].key &&
                $scope.main.formParams.flight.key === $scope.main.constants.flightOptions[0].key &&
                $scope.main.formParams.ride.key === $scope.main.constants.rideOptions[0].key);

            if ($scope.main.formParams.rideType.key === 'ANY' ||
                $scope.main.formParams.ride.key === 'ANY' ||
                $scope.main.formParams.ride.key === 'NO') {
                    $scope.main.formParams.disableNumPassengers = true;
                    $scope.main.formParams.numPassengers = null;
            }
            else {
                $scope.main.formParams.disableNumPassengers = false;
            }
        }
    );

    $scope.$watch("main.event", function (newValue, oldValue) {
        if (newValue === oldValue) {
            return;
        }
        filterChange += 1;
    });

    $scope.$watch("main.formParams.rideType", function (newValue, oldValue) {
        if (newValue === oldValue) {
            return;
        }
        $scope.pageChanged(1);
        filterChange += 1;
    });

    $scope.$watch("main.formParams.rt", function (newValue, oldValue) {
        if (newValue === oldValue) {
            return;
        }
        $scope.pageChanged(1);
        filterChange += 1;
    });

    $scope.$watch("main.formParams.numPassengers", function (newValue, oldValue) {
        filterChange += 1;
        // null != undefined -- don't update page if newValue is null, which it is on load
        if (!newValue || newValue === oldValue) {
            return;
        }
        $scope.pageChanged(1);
    });

    $scope.$watch("main.formParams.listingName", function (newValue, oldValue) {
        if (newValue === oldValue) {
            return;
        }
        $scope.pageChanged(1);
        filterChange += 1;
    });

    $scope.$watch("main.formParams.tofrom", function(newValue, oldValue) {
        if (newValue === oldValue) { return; }
        $scope.pageChanged(1);
        filterChange += 1;
    });

    // Make sure that hauling and ride cannot both be 'No'.
    // If you are taking neither passengers nor stuff it's not a rideshare!
    $scope.$watch("main.formParams.hauling", function(newValue, oldValue) {
        if (newValue === oldValue) { return; }
        if ($scope.main.formParams.hauling.key === 'NO' && $scope.main.formParams.ride.key === 'NO') {
            $scope.main.formParams.ride = $scope.main.constants.rideOptions[0]; // ANY
        }
        $scope.pageChanged(1);
        filterChange += 1;
    });

    $scope.$watch("main.formParams.ride", function(newValue, oldValue) {
        if (newValue === oldValue) { return; }
        if ($scope.main.formParams.ride.key === 'NO' && $scope.main.formParams.hauling.key === 'NO') {
            $scope.main.formParams.hauling = $scope.main.constants.haulingOptions[0]; // ANY
        }
        $scope.pageChanged(1);
        filterChange += 1;
    });

    $scope.$watch("main.formParams.flight", function(newValue, oldValue) {
        if (newValue === oldValue) { return; }
        $scope.pageChanged(1);
        filterChange += 1;
    });

    // Special hack using $swipe for slider to catch slider move events and don't update scroller until end event.
    (function() {
        $timeout(function() {
            $swipe.bind($('.input.low'),{
                // Scroll end event handler. See docs here: https://docs.angularjs.org/api/ngTouch/service/$swipe
                start: function(coords, event) {
                    event.stopPropagation();
                },
                end: function(event) {
                    $scope.$apply(function() {
                            filterChange += 1;
                            $scope.pageChanged(1);
                        }
                    );
                },
                cancel: function() {
                    //Reset ngModel value of widget to initial value here
                }
            });
        }, 100);
    })();

    $scope.$watch("main.formParams.location", function(newValue, oldValue) {
        if (newValue === oldValue) { return; }
//        var loc = $scope.main.formParams.location;
//        var ev = $scope.main.event;
        // calculate the distance between location and event -- as the crow flies
//        if (loc && typeof(loc) != "undefined" && typeof(loc) != "string" ) {
//            $scope.main.formParams.locationFormatted = loc.content_auto;
//            if (ev && typeof(ev) != "undefined") {
//                var lon1 = ev.location.location.coordinates[0]
//                var lat1 = ev.location.location.coordinates[1]
//                var lon2 = loc.location[0]
//                var lat2 = loc.location[1]
//                $scope.main.distance = $filter('dist')($scope.main.preferredDistanceUnit, lat1, lon1, lat2, lon2)
//            }
//
//        }
//        else {
//            $scope.main.formParams.locationFormatted = "";
//            $scope.main.distance = null
//        }
        filterChange += 1;
        $scope.pageChanged(1);
    });
}]);

// Create -OR- Edit a Rideshare
mod.controller('CreateController',['$scope', '$rootScope', '$state', '$stateParams', '$filter','$timeout', 'Rideshare', 'AutoLocation', 'Preference', 'MainScope', 'event', 'rideshare',
            function($scope, $rootScope, $state, $stateParams, $filter, $timeout, Rideshare, AutoLocation, Preference, MainScope, event, rideshare) {
    $scope.pageClass = "page-create";
    $scope.main = MainScope;
    $scope.main.event = event;
    $scope.useAirport = false;

    if ($stateParams.id && !rideshare) {
        $state.go('detail', {id: $stateParams.id});
    }

    Preference.query().$promise.then(
        function (prefs) {
            $scope.main.smokingOptions = $filter('getByRegex')('key', 'SMOKING', prefs);
            // This is just a convenience hack so we can use yes/no for smoking option display
            for (var i = 0; i < $scope.main.smokingOptions.length; i++) {
                var item = $scope.main.smokingOptions[i];
                if (item.key === 'SMOKING') {
                    item.yesOrNoKey = 'YES';
                }
                if (item.key === 'NON_SMOKING') {
                    item.yesOrNoKey = 'NO';
                }

            }
            $scope.main.disabledOption = $filter('getByProperty')('key', 'DISABLED', prefs);
            $scope.main.openMindedOption = $filter('getByProperty')('key', 'OPEN_MINDED', prefs);
        }
    );


    // This datepicker is a container for the Departure Date (dd) and Return Date (rd) objects.
    // The min date for dd is Today.  The min date for rd is whatever dd is set to. (You can't leave before you return
    // although exceptions will be made for a young lady named Bright!),
    // TODO for internationalization, setting starting-day?  The dateFormat ("fullDate") should be localized by Angular
    // TODO days_after_event should be equal to the settings.EVENT_VIABILITY_EXTENSION on backend, get value from there.
    var days_after_event = 5;
    $scope.datepicker = {
        'dd': {
            'open': function ($event) {
                $event.preventDefault();
                $event.stopPropagation();
                this.opened = true;
            },
            'minDate': new Date(),
            'maxDate': new Date(Date.parse(event.end_date) + days_after_event * 24 * 60 * 60 * 1000),
            'opened': false
        },
        'rd': {
            'open': function ($event) {
                $event.preventDefault();
                $event.stopPropagation();
                this.opened = true;
            },
            'maxDate': new Date(Date.parse(event.end_date) + days_after_event * 24 * 60 * 60 * 1000),
            'opened': false
        }
    };

    // expired prevents the creation of a new rideshare, or the editing of an existing one
    $scope.expired = $scope.datepicker.dd.minDate > $scope.datepicker.dd.maxDate;

    // Edit an existing Rideshare (it was already resolved)
    if(rideshare) {
        $scope.editing = true;

        //console.log('RESULT', rideshare)
        $scope.rs = rideshare;
	
        $scope.rs.earliest_departure = new Date($scope.rs.earliest_departure);
        $scope.rs.earliest_return = new Date($scope.rs.earliest_return);

        // converting the flight boolean to the flight object, and setting the useAirport value.
        // useAirport is used for the case when this is a flight /and/ it's an offer (in other words
        // its author is presumably a pilot who knows which airport he's flying to or from).
        $scope.rs.flight = $scope.main.realOptions.flightOptions[$scope.rs.flight ? 1 : 0];
        $scope.useAirport = $scope.rs.flight.key === 'YES';

        // Fix up the preferences -- m2m in database, but here we use tri-toggle and toggle.
        // The toggles use 0=false, 1=true and (for tri-toggle) undefined.
        try {
            // This will match either SMOKING or NON_SMOKING, but only one can be set
            var smoking = $filter('getByRegex')('key', 'SMOKING', $scope.rs.preferences)[0];
            $scope.smokingPref = smoking.key === 'SMOKING' ? 1 : 0;
        }
        catch(err) { $scope.smokingPref = undefined; }

        try { $scope.accommodateDisabled = $filter('getByRegex')('key', 'DISABLED', $scope.rs.preferences)[0] && 1; }
        catch(err) { $scope.accommodateDisabled = 0; }

        try { $scope.openMinded = $filter('getByRegex')('key', 'OPEN_MINDED', $scope.rs.preferences)[0] && 1 ; }
        catch(err) { $scope.openMinded = 0; }

        // This code modifies the representation of the Location, which is either a city or an airport,
        // to make it match what the typeahead uses.  That is, the result.location is a normal representation of
        // a City or Airport, but in this case, we want it to be an AutoCity or AutoAirport.  It may be possible
        // to change this in the API specifically when /editing/ an existing Rideshare object.
        // This process is reversed in submit().
        try {
            var loc = {};
            if ($scope.rs.location.city) {
                loc.text = $scope.rs.location.city.std_name;
                loc.id = $scope.rs.location.city.id;
            }
            else {
                loc.text = $scope.rs.location.airport.std_name;
                loc.id = $scope.rs.location.airport.id;
            }
            $scope.rs.location = loc;
        }
        catch(err){
            $scope.rs.location = null;
        }

    } else {
        // Creating a new Rideshare, starting with an empty one with default values...
        $scope.rs = {};
        $scope.editing = false;
        $scope.accommodateDisabled = 0;
        $scope.openMinded = 0;
        $scope.smokingPref = undefined;

        $scope.rs.event = $scope.main.event;
        $scope.rs.rtype = $scope.main.constants.rideTypeOptions[2];
        $scope.rs.ride = $scope.main.realOptions.rideOptions[1];
        $scope.rs.hauling = $scope.main.realOptions.haulingOptions[0];
        $scope.rs.round_trip = $scope.main.realOptions.roundTripOptions[1];
        $scope.rs.direction = $scope.main.realOptions.tofromOptions[0];
        $scope.rs.preferences = [];

        // Some preset form values we already know, based on the event start and end date
        $scope.rs.earliest_departure = $scope.rs.event.start_date;
        $scope.rs.earliest_return = $scope.rs.event.end_date;

        var now = new Date();
        var eventStart = new Date($scope.rs.event.start_date);
        var eventEnd = new Date($scope.rs.event.end_date);
        if (now > eventStart) {
            $scope.rs.earliest_departure = eventEnd;
        } else {
            $scope.rs.earliest_departure = eventStart;
        }
        if (now < eventEnd) {
            $scope.rs.earliest_return = eventEnd;
        }
        else {
            $scope.rs.earliest_return = now;
        }

        $scope.rs.flight = $scope.main.realOptions.flightOptions[0];
        $scope.rs.sms_notify = true;

        // TODO: Grabbing the values that were searched from formParams needs to be fixed, not crucial though
//        console.log('formParams: ', $scope.main.formParams.tofrom, $scope.main.formParams.rt)
//        console.log('scope.main.formParams:', $scope.main.formParams)
//        try {
//            if (typeof($scope.main.formParams.tofrom) === 'undefined') throw 'tofrom is undefined'
//            $scope.rs.direction = $scope.main.constants.tofromOptions[$scope.main.formParams.tofrom.id]
//        } catch(err) {
//            console.log('error', err)
//            $scope.rs.direction = $scope.main.constants.tofromOptions[0]
//        }
//        try {
//            if (typeof($scope.main.formParams.rt) === 'undefined') throw 'rt is undefined'
//            if ($scope.main.formParams.rt.id >= 0) { $scope.rs.round_trip = $scope.main.realOptions.roundTripOptions[$scope.main.formParams.rt.id] }
//        } catch(err) {
//            console.log('error', err)
//            $scope.rs.round_trip = $scope.main.realOptions.roundTripOptions[1]
//        }
//        try {
//            if (typeof($scope.main.formParams.rideType) === 'undefined') throw 'rideType is undefined'
//            if ($scope.main.formParams.rideType.id >= 0) { $scope.rs.rtype = $scope.main.realOptions.rideTypeOptions[$scope.main.formParams.rideType.id] }
//        } catch(err) {
//            console.log('error', err)
//            $scope.rs.rtype = $scope.main.realOptions.rideTypeOptions[1]
//        }
//        try {
//            if (typeof($scope.main.formParams.hauling) === 'undefined') throw 'hauling is undefined'
//            if ($scope.main.formParams.hauling.id >= 0) { $scope.rs.hauling = $scope.main.realOptions.haulingOptions[$scope.main.formParams.hauling.id] }
//        } catch(err) {
//            console.log('error', err)
//            $scope.rs.hauling = $scope.main.realOptions.haulingOptions[1]
//        }
//        try {
//            if (typeof($scope.main.formParams.ride) === 'undefined') throw 'ride is undefined'
//            if ($scope.main.formParams.ride.id >= 0) { $scope.rs.ride = $scope.main.realOptions.rideOptions[$scope.main.formParams.ride.id] }
//        } catch(err) {
//            console.log('error', err)
//            $scope.rs.ride = $scope.main.realOptions.rideOptions[0]
//        }
    }


    $scope.cancel = function () {
        history.back();
    };

    $scope.submit = function() {
        // Copy the object.
        var rs = JSON.parse(JSON.stringify($scope.rs));

        // Convert the preferences (smoking, disabled, open-minded) to the m2m version that the API/db accepts
        // *replacing* the preferences array, so that we don't end up with duplicate preferences when we push.
        rs.preferences = [];
        if ($scope.smokingPref === 0 || $scope.smokingPref === 1) {
            rs.preferences.push($scope.main.smokingOptions[$scope.smokingPref]);
        }
        if ($scope.accommodateDisabled) {
            rs.preferences.push($scope.main.disabledOption);
        }
        if ($scope.openMinded) {
            rs.preferences.push($scope.main.openMindedOption);
        }

        // only add the '+' if there is a phone number
        if (rs.contact_phone) { rs.contact_phone = rs.contact_phone.replace(/^[+]*/, '+'); }

        // The API rejects 0 -- it must be >= 1
        if (rs.riders === 0) { rs.riders = null; }

        // The $scope.rs.location object is either a city or an airport.
        // Here we set location to city or airport wrapper, so the api knows what it's getting.
        rs.location = {};
        if (rs.flight.key === 'YES' && rs.rtype.key === 'OFFER') {
            rs.location.airport = $scope.rs.location;
            rs.location.airport.id = $scope.rs.location.id;
            rs.location.city = null;
        }
        else {
            rs.location.city = $scope.rs.location;
            rs.location.city.id = $scope.rs.location.id;
            rs.location.airport = null;
        }

        // Change the flight value to boolean for db (must come after the rs.flight.key test, above)
        rs.flight = rs.flight.key === 'YES';

        // TODO Make modal timeout handling consistent
        var doModalTimeout = function () {
            $timeout(function(){
                $rootScope.$emit('modal');
                $scope.main.apiResponse = null;
            }, 5000);
        };

        // Now update the existing Rideshare, or create a new one
        $scope.main.apiResponse = null;
        $rootScope.$emit('modal', '/static/rideshare_frontend/partials/feedback_modal.html');
        if ($stateParams.id) {
            Rideshare.update({ id:$stateParams.id }, rs,
                function(res) {
                    $scope.main.apiResponse = {
                        "success": true,
                        "headerKey": "SUCCESS",
                        "messageKey": "LISTING_SUCCESS_MSG",
                        "operation": "update"

                    };
                    doModalTimeout();
                    $state.go('detail', {id: res.id});
                },
                function(res) {
                    $scope.main.apiResponse = {
                        "success": false,
                        "headerKey": "FAILURE",
                        "messageKey": "GENERAL_FAILURE_MSG"
                    };
                    doModalTimeout();
                });
        }
        else {
            Rideshare.save(rs).$promise.then(
                function(res) {
                    $scope.main.apiResponse = {
                        "success": true,
                        "headerKey": "SUCCESS",
                        "messageKey": "LISTING_SUCCESS_MSG",
                        "operation": "create"
                    };
                    doModalTimeout();
                    $state.go('detail', {id: res.id});
                },
                function(res) {
                    $scope.main.apiResponse = {
                        "success": false,
                        "headerKey": "FAILURE",
                        "messageKey": "GENERAL_FAILURE_MSG"
                    };
                    doModalTimeout();
                });
        }
    };

    // Query API for a location (city or airport) with partial string using typeahead
    // TODO Combine getLocation() functions in a service for DRY
    $scope.getLocation = function (val) {
        // loop over ridetypes and if this is a pilot offering a flight, have them specify an airport
        // in all other cases, it will be cities.  This might be cleaner if we just separate out the hauling!
        return AutoLocation.get(val, $scope.useAirport, $scope.main.event).then(
            function (res) {
                var locations = [];
                angular.forEach(res.data.results, function (item) {
                    locations.push(item);
                });
                console.log('locations', locations);
                return locations;
            }
        );
    };

    // If this is a Flightshare AND it's a Request, then location is a City. If it's an Offer then location is an Airport.
    // Pilots presumably know the specific airport to/from which they are flying, passengers not necessarily.
    // Also, if it's a flight then you are taking passengers, not hauling stuff (check to make sure that's what is wanted).
    $scope.$watchGroup(['rs.rtype','rs.flight'], function(newVals, oldVals) {
        if (typeof(newVals) === 'undefined' ||  !$scope.rs ||
            (newVals[0].id === oldVals[0].id && newVals[1].id === oldVals[1].id)) {
            return;
        }
        if ($scope.rs.flight.key === 'YES') {
            if ($scope.rs.rtype.key === 'OFFER') {
                $scope.useAirport = true;
                $scope.rs.location = null;
            }
            else {
                $scope.useAirport = false;
                $scope.rs.location = null;
            }
            $scope.rs.hauling = $scope.main.realOptions.haulingOptions[0];  // NO
            $scope.rs.ride = $scope.main.realOptions.rideOptions[1]; // YES
        }
        else {
            $scope.useAirport = false;
            $scope.rs.location = null;
        }
    });

    // Only certain combinations of ride, riders and hauling are allowed!
    $scope.$watch('rs.ride', function(newVal, oldVal) {
        if (typeof(newVal) === 'undefined' || !$scope.rs) { return; }
        if (newVal.key === 'NO') {
            $scope.rs.riders = null;
            if ($scope.rs.hauling.key === 'NO') {
                $scope.rs.hauling = $scope.main.realOptions.haulingOptions[1]; // YES
            }
            if ($scope.rs.ride.key === 'NO' &&  $scope.rs.flight.key === 'YES') {
                $scope.rs.ride = $scope.main.realOptions.rideOptions[1]; // YES
            }
        }

    });
    $scope.$watch('rs.hauling', function(newVal, oldVal) {
        if (typeof(newVal) === 'undefined' || !$scope.rs) { return; }
        if (newVal.key === 'NO') {
            if ($scope.rs.ride.key === 'NO') {
                $scope.rs.ride = $scope.main.realOptions.rideOptions[1]; // YES
                $scope.rs.riders = null;
            }
        }
    });

    // This keeps the return date >= the departure date, setting corresponding minDate in datepicker calendar
    $scope.$watchGroup(['rs.earliest_departure', 'rs.round_trip'], function(newVals, oldVals) {
        if (typeof(newVals) === 'undefined' ||  !$scope.rs ||
            (newVals[0] === oldVals[0] && newVals[1].id === oldVals[1].id)) {
            console.log('watchgroup', newVals, oldVals);
            return;
        }
        $scope.datepicker.rd.minDate = new Date($scope.rs.earliest_departure);
        // Converting strings to dates here to avoid Moment deprecation warning described here:
        // https://github.com/moment/moment/issues/1407
        //$scope.rs.earliest_return = new Date($scope.rs.earliest_return).toISOString();
        //$scope.rs.earliest_departure = new Date($scope.rs.earliest_departure).toISOString();
        if ($scope.rs.earliest_departure > $scope.rs.earliest_return) {
            $scope.rs.earliest_return = $scope.rs.earliest_departure;
        }
    });
}]);
