import os
from setuptools import setup

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "rideshare-frontend",
    version = "0.1.0",
    author = "Liam Kirsher",
    author_email = "liamk@numenet.com",
    description = "Django/AngularJS front end to Rideshare application",
    license = "BSD",
    keywords = "django angular rideshare",
    #url = "http://packages.python.org/an_example_pypi_project",
    packages=['rideshare_frontend'],
    include_package_data=True,
    install_requires=['django==1.10.4'],
    long_description=read('README.md'),
    zip_safe=False,
    classifiers=[
        "Development Status :: 3 - Alpha",
        "License :: OSI Approved :: BSD License",
        "Intended Audience :: Developers",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 2.7",
        "Framework :: Django",
        "Framework :: Django :: 1.10",
        "Topic :: Software Development :: Libraries :: Python Modules",
    ],
)
