# Rideshare Frontend

**Rideshare Frontend** is a Open Source Django application that delivers an AngularJS client.

The AngularJS client can be used independently of Django, with minor modifications.

It connects to a server running
[Rideshare backend](https://gitlab.com/liamk7/rideshare).  For more documentation
about Rideshare, go there.

## Features

The Rideshare Frontend AngularJS application allows the user to create *Rideshares*,
that is, offers and requests to share a vehicle with someone going to an event.

* Language support, with current languages: English, Spanish and French.
* International phone number parsing.
* Gravatar images to represent users.
* Bootstrap for responsive design.

## Installation

#### On same server
Rideshare frontend can be served by the same server running Rideshare.

Because the package relies on various JavaScript modules, the preferred way to install it in production is to create a source distribution and then unpack that where you want it.

For development, try the "editable install" option, -e, from within your virtual environment.

1. `git clone git@gitlab.com:liamk7/rideshare-frontend.git`
1. `pip install -e /path/to/rideshare-frontend`

Or, just create a link to the rideshare_frontend directory in your Django project's base directory.

Then go into the rideshare_frontend directory and follow the build instructions in the README file.

#### Somewhere else

You can also deliver the Rideshare frontend from a different server.
In this case, you will need to configure CORS settings on the Rideshare backend server.

See the application [README](./rideshare_frontend/README.md) or more information on installation.
## Future plans

Rideshare frontend is usable, but is nearing the end of its days in its current incarnation.

1. It uses AngularJS.  That has been superseded by Angular 2.0, which
would require substantial rewrites.  Meanwhile, React is currently
ascendant among developers and is being deployed elsewhere in the Burning
Man ecosystem.  Thus it makes sense to move to React.

2. It uses gulp and bower, which are being phased out.  It should use
something like Webpack.

A few of the features that would be nice to add require changes to both the frontend and the backend.  See the Rideshare Roadmap for more specifics.

We are anticipating building a new and improved version in [React](https://reactjs.org/) 
with the same functionality and a lot of enhancements.  Notes on what that will look like
can be found [here](https://gitlab.com/liamk7/rideshare/docs/ROADMAP.md)

If you are a React programmer who loves Open Source and wants to get involved, please get in touch!

## License

This project follows the Apache 2.0 license. See the [LICENSE](./LICENSE.md) file for details.
